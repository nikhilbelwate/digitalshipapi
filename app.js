
var WebSocketUtil=require('./model/WebSocketUtil');
const express = require('express');
const expressWs = require('express-ws');
var app = express();
WebSocketUtil.wsInstance=expressWs(app);

var cors = require('cors');
var config= require('./config.json');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var TokenManager = require('./logic/TokenManager');
var ShipUtil = require('./model/ShipUtil');
var StatsUtil = require('./model/StatsUtil');
var EngineUtil = require('./reducers/EngineUtil');
var DBUtil=require("./db/DBUtil");
var DLUtil=require("./db/DLUtil");

DLUtil.getCountOftugboat_data_raw();
ShipUtil.init();
StatsUtil.init();
EngineUtil.init();
DBUtil.init();
var main=require("./urls/main");
var index = require('./urls/index');
var blob = require('./urls/blob');
var shipController= require('./urls/ship');
var stats= require('./urls/stats');
var alerts= require('./urls/alerts');
var schedule= require('./urls/schedule');
var monitoring= require('./urls/monitoring');
var resources= require('./urls/resources');
var SIMship= require('./urls/simship');
var performance= require('./urls/performance');
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser()); 

//setting middleware
app.use(express.static(__dirname + '/public')); //Serves resources from public folder
//Serves all the request which includes /images in the url from Images folder
app.use('/images', express.static(__dirname + '/public/images'));
app.use('/ws',main);
app.use('/',index);
app.use('/blob', blob);
app.use('/api/ships',shipController);
app.use('/api/stat',stats);
app.use('/alerts',alerts);
app.use('/api/schedule',schedule);
app.use('/api/monitoring',monitoring);
app.use('/api/resources',resources);
app.use('/api/SIM',SIMship);
app.use('/api/performance',performance);
app.ws('/', function(ws, req) {

  
  //on connection
  ws.send(JSON.stringify(ShipUtil.getData()));
  ws.cid=0;
  WebSocketUtil.saveClient(0,ws);
  
  ws.on('message', (message) => { 
        
    ShipUtil.setShips(JSON.parse(message));
    
    WebSocketUtil.broadcast(JSON.stringify(ShipUtil.getShips()),0);
  
  });

  ws.on('close', () => {
    WebSocketUtil.remove(0);
  });
});

ShipUtil.AppStartTime=new Date();


//const schedule_timer = require('node-schedule');
//For every 1 min
//schedule_timer.scheduleJob('*/1 * * * *', function(){
/*  console.log('The answer to life, the universe, and everything!');
  TokenManager.fetchArasToken();
});*/
module.exports = app;
