const e = require("express");
const mqtt = require("mqtt");
class MQTTConnector {

    constructor() {
        this.Message = new Array();
        try {

            this.client = mqtt.connect("mqtt://test.mosquitto.org");
            console.log("mqtt://172.16.176.137");
            this.client.on("connect", () => {
                console.log("connected");
            });
        } catch (error) {
            console.log(error);
        }
    }
    
    publishMessage(topic, message) {
        this.client.publish(topic, message);
    }
    subscribeTopic(topic, callback) {
        //topic ="dsiss/#";
        if (topic === null || topic === undefined) {
            console.log("topic is null | Not subscribing");
        } else {
            if (callback === null || callback === undefined) {
                callback = (message) => {
                    console.log(message);
                }
            }
            this.client.subscribe(topic, (err) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("subscribed");
                    this.client.on("message", (topic, message) => {
                        callback(message);
                        console.log("received message %s %s", topic, message);
                    });
                }
            });
        }

    }
    unsubscribeTopic(topic) {
        this.client.unsubscribe(topic);
    }
    end() {
        this.client.end();
    }
}

module.exports = MQTTConnector;