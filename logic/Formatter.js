var LocationUtil=require("../reducers/LocationUtil");
var EngineUtil=require("../reducers/EngineUtil");
class Formatter{
    static apply(ship){
        //Code to apply ship
        LocationUtil.convertLatitude(ship?.currentPosition?.gps?.latitude);
        LocationUtil.convertLongitude(ship?.currentPosition?.gps?.longitude);
        LocationUtil.convertcourseAngle(ship?.currentPosition?.courseOverGround);
        //console.log(ship["currentPosition"]);
        
        EngineUtil.mergeArray(ship.operatingParameters);
        EngineUtil.formatComputedValues(ship.computedParameters);
        Object.entries(ship?.history).map(([k,item])=>{
            item.value.sort((a, b) => {
                const timeStampA = parseInt(a.timeStamp);
                const timeStampB = parseInt(b.timeStamp);
                return timeStampA - timeStampB;
              });
            ship.history[k].value=item.value;
        });
        ship["vesselTotal"]["totalEngineHours"]["displayValue"]=ship?.vesselTotal?.totalEngineHours?.value;
        ship["vesselStatus"]["estimateEndTime"]=new Date(parseInt(ship?.vesselStatus?.estimateEndTime)*1000);
        ship["vesselStatus"]["estimateStartTime"]=new Date(parseInt(ship?.vesselStatus?.estimateStartTime)*1000);
        return ship;
    }
}
    
module.exports=Formatter;