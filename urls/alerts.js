const express = require('express');
var router = express.Router();
var WebSocketUtil = require("../model/WebSocketUtil");
var ShipUtil = require('../model/ShipUtil');
var DBUtil = require("../db/DBUtil");
const AlertUtil = require('../model/AlertUtil');
class Alert{
    constructor(alertData) {
        this.vessel_id = alertData?.vessel_id;
        if (!alertData.vessel_id) {
            this.mmis = alertData?.mmis;
            this.vessel_id = ShipUtil.getShipID(this.mmis);
            if (!this.vessel_id) {
                throw new Error("Vessel ID and Vessel MMIS not present in alert data");
            }
        }
        this.component_id = alertData?.component_id;
        if (this.component_id === undefined || this.component_id === null || this.component_id < 1) {
            this.component_id = 0;
        }
        this.message = alertData?.message;
        this.extra=alertData?.extra!==undefined?alertData?.extra:'';
        this.info = alertData?.info;
        this.generated_at=new Date().getTime();
        this.is_active = alertData?.is_active;
        this.value=alertData.value===undefined?this.is_active?1:0:parseInt(alertData.value);
        this.priority = alertData?.priority;
        if (this.priority ===1 || this.priority === 2) {
            this.type = "critical";
        } else if (this.priority < 5) {
            this.type = "high";
        } else if(this.priority < 8) {
            this.type = "medium";
        }else if(this.priority ===8 || this.priority === 9){
            this.type = "low";
        }else{
            this.type= "info";
        }
    }
}

router.post("/history",async function (req, res, next) {
    let params = req.body;
    if(params?.vessel_id>0){

        if(params?.component_id>=0){
            let result=await DBUtil.getAlertHistory(params.vessel_id, params.component_id);
            res.json(result);    
        }else{ 
            next(new Error("component_id missing in request for history"))
        }
    }else{
        throw new Error("Need vessel_id for history");
    }

});
router.post('/generate', async function (req, res, next) {
    let alert = req.body;
    try {
        alert = new Alert(alert);
        console.log(alert);
        //DBUtil.saveAlert(alert);
        if(AlertUtil.addNewAlert(alert)){
            console.log(AlertUtil.AlertMap);
            let allAlertsOfShip=AlertUtil.getShipAlerts(alert.vessel_id);
            WebSocketUtil.broadcastAlerts(JSON.stringify(allAlertsOfShip,AlertUtil.replacer), alert.vessel_id);
            WebSocketUtil.broadcastAlerts(JSON.stringify(AlertUtil.getAllAlerts()), 0);
            DBUtil.saveAlert(alert);
            //Logic yet to complete
            res.send(JSON.stringify(allAlertsOfShip,AlertUtil.replacer));
        }else{
            res.send({errors:JSON.stringify(AlertUtil.errors)})
        }
        /*let ship = ShipUtil.getShip(alert.vessel_id);
        let is_new_alert = true;
        ship["alert"] = ship?.alert?.filter((alert_obj, index) => {
            if (alert_obj?.component_id === alert?.component_id) {
                is_new_alert = false;
                if (alert?.is_active === false) {
                    return false;
                } else {
                    alert_obj["message"] = alert?.message + "<br/>" + alert_obj?.message;
                    alert_obj["type"] = alert?.type;
                    alert_obj["info"] = Object.assign(alert_obj?.info, alert?.info);
                    if (alert_obj?.priority < alert?.priority) {
                        alert_obj["priority"] = alert?.priority;
                        alert_obj["class"] = alert?.class;
                    }
                }
            }
            return true;
        });
        if (is_new_alert) {
            ship["alert"].push(alert);
        }
        ShipUtil.setShip(ship);
        WebSocketUtil.broadcast(ship, alert.vessel_id);
        //Logic yet to complete
        res.json(ShipUtil.getShip(alert.vessel_id)["alert"]);*/
    } catch (err) {
        next(err);
    }
});
router.ws('/ws/:sId', function(ws, req) {
    console.log("In alert Websocket");
    let cid=parseInt(req.params.sId);
    ws.sId=cid;
   
    ws.on('open',function(){
        console.log('open alert Websocket');
    });

    ws.on("error",function(err){
        console.log(err);
    });
    if(cid===0)
        ws.send(JSON.stringify(AlertUtil.getAllAlerts()));
    else
        ws.send(JSON.stringify(AlertUtil.getShipAlerts(cid),AlertUtil.replacer));
    //console.log(ShipUtil.ships);
    WebSocketUtil.saveAlertClient(cid,ws);

});
router.get('/all/:sId', function(req, res) {

    let sId=parseInt(req.params.sId);
    res.send(JSON.stringify(AlertUtil.getShipAlerts(sId),AlertUtil.replacer))

});
router.get('/all', function(req, res) {

    res.send(JSON.stringify(AlertUtil.getAllAlerts()))

});


module.exports = router;