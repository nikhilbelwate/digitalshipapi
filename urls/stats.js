var express = require("express");
var StatsUtil = require("../model/StatsUtil");
const ShipUtil = require("../model/ShipUtil");
const DBUtil = require("../db/DBUtil");
var router = express.Router();

router.get("/ships", function (req, res, next) {
  res.json(getDataForPiCharts(StatsUtil.getStats().ships));
});
router.get("/ship/:sId", function (req, res, next) {
  res.json(StatsUtil.getStats().ships[req.params.sId]);
});
router.get("/getfleetdata", async function(req, res, next){
  let result=await DBUtil.getFleetStatus();
  result.sort((a, b) => b.value - a.value);
  res.json(result);
});
router.get("/getfleetdata1", function (req, res, next) {
  let allShips = null;
  //Object.entries(ShipUtil.getShips()).map(([k, s]) => 
  for (let s of Object.values(StatsUtil.getStats().ships)) {
    //console.log("s",s);
    if (allShips === null) {
      allShips = Object.assign({}, s);
    } else {
      allShips = StatsUtil.sumOfObjects(allShips, s);
    }
  }
  console.log(allShips);

  let data = [
    { name: "Underway", value: StatsUtil.getMonthlyPercentage(allShips.vesselOperationStatus.underway) },
    { name: "Idle-Crew-On", value: StatsUtil.getMonthlyPercentage(allShips.vesselOperationStatus.idleCrewOn) },
    { name: "Maintenance", value: StatsUtil.getMonthlyPercentage(allShips.vesselOperationStatus.maintenance) },
    { name: "Idle-Crew-Off", value: StatsUtil.getMonthlyPercentage(allShips.vesselOperationStatus.idleCrewOff) },
  ];
  data.sort((a, b) => b.value - a.value);
  res.json(data);
});
router.post("/update", function (req, res, next) {
  let data = req.body;
  StatsUtil.setStats(data);
  res.json(StatsUtil.getStats());
});
router.get("/maintenanceCounts", async function (req, res, next) {
  let result = {
    "certificates_due": await DBUtil.getMaintenanceDueIn60Days(),
    "scheduled": await DBUtil.getScheduledNext30days(),
    "overdue": await DBUtil.getOverdueScheduledCount()
  }
  res.json(result);
});
router.get("/saftyCounts", function (req, res, next) {
  let safety = {
    "30daysincidents": {
      "value": 1,//for now hardcode value, then get it from DBUtil
      "unit": "",
      "direction": "flat",
      "status": "same"
    },
    "lossdays": {
      "value": 410, //for now hardcode value, then get it from DBUtil
      "unit": "days",
      "direction": "none",
      "status": "none"
    }

  }
  res.json(safety);
});
router.get("/carbonEmissionCounts", function (req, res, next) {
  let environment = {
    "emissions": {
      "value": 320,//for now hardcode value, then get it from DBUtil
      "unit": "Ton",
      "direction": "up",
      "status": "good"
    },
    "spills": {
      "value": 2, //for now hardcode value, then get it from DBUtil
      "unit": "Ton",
      "direction": "down",
      "status": "bad"
    }

  }
  res.json(environment);
});
function getDataForPiCharts(ships) {
  let allShips = null;

  Object.entries(ships).map(([k, s]) => {
    //console.log("s",s);
    if (allShips === null) {
      allShips = { ...s };
    } else {
      allShips = StatsUtil.sumOfObjects(allShips, s);
    }
  });

  let piaArray = [];
  let info = {};
  info["name"] = "Certificates Due By Ship";
  info["id"] = 1;
  info["data"] = [
    { name: "Next 3 Months", value: allShips.vesselMaintenance.certNext3M, ships: [] },
    { name: "Next 6 Months", value: allShips.vesselMaintenance.certNext6M, ships: [] },
    { name: "Next 9 Months", value: allShips.vesselMaintenance.certNext9M, ships: [] },
  ];

  piaArray[0] = info;
  info = {};
  info["name"] = "Open Work Orders";
  info["id"] = 2;
  info["data"] = [
    {
      name: "Preventive Maintenance",
      value: allShips.vesselMaintenance.PMJobs,
      ships: []
    },
    {
      name: "Non-Preventive Maintenance",
      value: allShips.vesselMaintenance.NonPMJob,
      ships: []
    },
  ];

  piaArray[1] = info;
  info = {};
  info["name"] = "Overdue Maintenance Jobs";
  info["id"] = 3;
  info["data"] = [
    { name: "Standard Job", value: allShips.vesselMaintenance.standardJob, ships: [] },
    {
      name: "Work Order by Crew",
      value: allShips.vesselMaintenance.workOrderByCrew,
      ships: []
    },
    {
      name: "Standard Operation",
      value: allShips.vesselMaintenance.standardOperation,
      ships: []
    },
    {
      name: "Standard Requisition",
      value: allShips.vesselMaintenance.standardRequistion,
      ships: []
    },
  ];
  piaArray[2] = info;
  info = {};
  info["name"] = "Fleet Status Last Month";
  info["id"] = 4;
  info["data"] = [
    { name: "Underway", value: StatsUtil.getMonthlyPercentage(allShips.vesselOperationStatus.underway), ships: [] },
    { name: "Idle - Crew On", value: StatsUtil.getMonthlyPercentage(allShips.vesselOperationStatus.idleCrewOn), ships: [] },
    { name: "Maintenance", value: StatsUtil.getMonthlyPercentage(allShips.vesselOperationStatus.maintenance), ships: [] },
    { name: "Idle - Crew Off", value: StatsUtil.getMonthlyPercentage(allShips.vesselOperationStatus.idleCrewOff), ships: [] },
  ];
  piaArray[3] = info;
  Object.entries(ships).map(([k, s]) => {
    piaArray[0].data[0].ships.push({ shipname: s.vessel, value: s.vesselMaintenance.certNext3M });
    piaArray[0].data[1].ships.push({ shipname: s.vessel, value: s.vesselMaintenance.certNext6M });
    piaArray[0].data[2].ships.push({ shipname: s.vessel, value: s.vesselMaintenance.certNext9M });
    piaArray[1].data[0].ships.push({ shipname: s.vessel, value: s.vesselMaintenance.PMJobs });
    piaArray[1].data[1].ships.push({ shipname: s.vessel, value: s.vesselMaintenance.NonPMJob });
    piaArray[2].data[0].ships.push({ shipname: s.vessel, value: s.vesselMaintenance.standardJob });
    piaArray[2].data[1].ships.push({ shipname: s.vessel, value: s.vesselMaintenance.workOrderByCrew });
    piaArray[2].data[2].ships.push({ shipname: s.vessel, value: s.vesselMaintenance.standardOperation });
    piaArray[2].data[3].ships.push({ shipname: s.vessel, value: s.vesselMaintenance.standardRequistion });
    piaArray[3].data[0].ships.push({ shipname: s.vessel, value: s.vesselOperationStatus.underway });
    piaArray[3].data[1].ships.push({ shipname: s.vessel, value: s.vesselOperationStatus.idleCrewOn });
    piaArray[3].data[2].ships.push({ shipname: s.vessel, value: s.vesselOperationStatus.maintenance });
    piaArray[3].data[3].ships.push({ shipname: s.vessel, value: s.vesselOperationStatus.idleCrewOff });
  });

  return piaArray;
}
module.exports = router;
