var express = require('express');
var router = express.Router();
var DBUtil=require("../db/DBUtil");
const axios = require('axios');
const https = require('https');
const TokenManager = require('../logic/TokenManager');

router.get("/getArasToken",async function(req, res, next){
    let token =await TokenManager.fetchArasToken();
    res.json({token:token});
});

router.get('/getComponentDocFileId/:vId/:cId', async function(req, res, next) {
    let vId=req.params.vId;
    let component_id = req.params.cId;
    let fileArray=await DBUtil.getComponentDocFileId(vId,component_id);
    res.json(fileArray);
});

router.get('/getFile/:fileId', async function getFile(req,res,next){
    let url = "https://van-ss-arasmro.seaspan.com/plmvsy/server/odata/File('"+req.params.fileId+"')/$value";
    let data = new URLSearchParams();
    
    let config = {
        headers: {  
          'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.62',
          //'Content-Type': 'application/x-www-form-urlencoded'
          'authorization': "Bearer "+TokenManager.Token.access_token,
          responseType: 'arraybuffer'
            //responseType: 'blob'
        }
      };
    axios.defaults.httpsAgent=new https.Agent({
        rejectUnauthorized:false
    });
    try{
    let response=await axios.get(url, config)
    console.log(response);
    const pdfBlob = Buffer.from(response.data, 'binary');
    res.setHeader('Content-Type', 'application/pdf');
    res.setHeader('Content-Disposition', 'attachment; filename=downloaded.pdf');
    res.send(pdfBlob);
    }catch(error){
        //console.error(error);
        if(error?.response?.status===401){
            console.log("Token out");
            let token =await TokenManager.fetchArasToken();
            if(token!=undefined && token!=""){
                getFile(req,res,next);
            }else{
                next(error);
            }
        }else{
        next(error);
        }
    }
});
module.exports = router;
