const express = require('express');
const WebSocket = require('ws');
var router = express.Router();
var WebSocketUtil =require("../model/WebSocketUtil");
var ShipUtil = require('../model/ShipUtil');

//var initData = require('../initdata/initdata.json');
//ShipUtil.setShips(initData);

router.ws('/main/:sId', function(ws, req) {
    console.log("In main Websocket");
    let cid=parseInt(req.params.sId);
    ws.sId=cid;
   
    ws.on('open',function(){
        console.log('open');
    });

    ws.on("error",function(err){
        console.log(err);
    });

    ws.on("message", function(msg) {
        console.log("recived msg");
        let data=JSON.parse(msg);
        if(data.hasOwnProperty("ships"))
            ShipUtil.setShips(data);
        else
            ShipUtil.setShip(data);
        
        WebSocketUtil.broadcast(ShipUtil.getShip(ws.sId),ws.sId);
        WebSocketUtil.broadcast(ShipUtil.getData(),0);
    });
    /*if(WebSocketUtil.CLIENTS.get(cid).length>1){
        
        let tmpShip=ShipUtil.getShip(cid);
    setInterval(() => {
        WebSocketUtil.broadcast(ShipUtil.getShip(ws.sId),ws.sId);
    }, 10000);
    }*/
    //console.log(ws);
    ws.send(JSON.stringify(ShipUtil.getShip(cid)));
    //console.log(ShipUtil.ships);
    WebSocketUtil.saveClient(cid,ws);

});
router.post("/modify/:sId",function(req,res,next){
        let data=req.body;
        if(data.hasOwnProperty("ships"))
            ShipUtil.setShips(data);
        else
            ShipUtil.setShip(data);
        WebSocketUtil.broadcast(ShipUtil.getShip(req.params.sId),req.params.sId);
        WebSocketUtil.broadcast(ShipUtil.getData(),0);
        res.send(ShipUtil.getShip(req.params.sId));
});
router.get("/health",function(req,res){
    let health={
        "Source": "Exchange",
        "Start Time": ShipUtil.AppStartTime,
        "Up Time": (process.uptime()*1000)
    }
    res.send(health);
});
router.get("/lastUpdate/:sId",function(req,res,next){
    let now=new Date().getTime()/1000;
    let last=parseInt(ShipUtil.getShip(req.params.sId)?.currentPosition?.lastUpdate);
    let timeDiff=now-last;
    let secondsPassed = Math.floor(timeDiff);
    let updates= {
        "Vessel_id": req.params.sId,
        "Latest Update": ShipUtil.getShip(req.params.sId)?.currentPosition?.lastUpdate,
        "relevant":secondsPassed,
        "Source": "Exchange"
    };
    res.send(updates);
});
router.get("/getUserCount",function(req,res,next){
    let shipId=0;
    if(req.query.sId!==undefined && req.query.sId!==null){
        shipId =parseInt(req.query.sId);
    }
        if(WebSocketUtil.CLIENTS.has(parseInt(shipId))){
            let clientArray = WebSocketUtil.CLIENTS.get(shipId);
                //console.log(clientArray?clientArray.length:0);
                res.send({count:clientArray?clientArray.length:0});
        }else{
            res.send({count:0});
        }   
});
module.exports = router;
