const express = require('express');
const WebSocket = require('ws');
var router = express.Router();
var WebSocketUtil =require("../model/WebSocketUtil");
var ShipUtil = require('../model/ShipUtil');
var DBUtil=require('../db/DBUtil');
//var initData = require('../initdata/initdata.json');

//ShipUtil.setShips(initData);
router.get('/', function (req, res, next) {

  res.json(ShipUtil.getShips());
});

router.get('/:sId', function (req, res, next) {
    res.header('Cache-Control', 'no-cache');
    res.json(ShipUtil.getShip(req.params.sId));
});

router.get("/allenginesload/:vId",async function(req,res,next){
//hard coded for two engines
  
  let start=req.query.start;
  let end=req.query.end;
  console.log(req.params.vId +"   "+start+"   "+end);
  res.json(await DBUtil.getEngineLoadRecords(req.params.vId,start,end));
});

router.get('/getProperties/:name', function(req,res,next){
    res.json(Array.from(DBUtil.porpertyMap.keys()));
});
router.post("/getTimelyValues/:vId",async function(req,res,next){
  //hard coded for two engines
    
    let start=req.body.start;
    let end=req.body.end;
    let offset=req.body.offset;
    if(offset===undefined)
      offset=0;
    if(end===null || end===undefined){
      end=new Date();
      
      if(start===null || start===undefined){
        let d=end.getDate()===1?28:end.getDate()-1;
        start=(end.getMonth()+1)+"/"+d+"/"+end.getFullYear();
      }
      end=(end.getMonth()+1)+"/"+end.getDate()+"/"+end.getFullYear();
    }
    let selectedProps=req.body.selected;
    if(selectedProps===undefined){
      selectedProps=Array.from(DBUtil.porpertyMap.keys());
    }
    console.log(req.params.vId +"   "+start+"   "+end+"  "+selectedProps);
    res.json(await DBUtil.getTimeAndData(req.params.vId,start,end,offset,selectedProps));
  });
  
/*
router.ws('/:sId', function(ws, req) {
    console.log("In maps Websocket");
    let cid=req.params.sId;
    ws.sId=req.params.sId;
    WebSocketUtil.saveClient(cid,ws);
    
    ws.on('message', function(msg) {
      ShipUtil.setShip(JSON.parse(msg));
      console.log(msg);
      WebSocketUtil.broadcast(ShipUtil.getShip(ws.sId),ws.sId);
      WebSocketUtil.broadcast(ShipUtil.getData(),0);
    });
    if(WebSocketUtil.CLIENTS.get(cid).length>1){
        
        let tmpShip=ShipUtil.getShip(cid);
    setInterval(() => {
        WebSocketUtil.broadcast(ShipUtil.getShip(ws.sId),ws.sId);
    }, 10000);
    }
    ws.send(JSON.stringify(ShipUtil.getShip(ws.sId)));
});
*/

router.ws('/ws', function(ws, req) {
      
  //on connection
  ws.send(JSON.stringify(ShipUtil.getData()));
  ws.cid=0;
  WebSocketUtil.saveClient(0,ws);
  
  ws.on('message', (message) => { 
        
    ShipUtil.setShips(JSON.parse(message));
    
    WebSocketUtil.broadcast(JSON.stringify(ShipUtil.getShips()),0);
  
  });

  ws.on('close', () => {
    WebSocketUtil.remove(0);
  });
});

module.exports = router;
