var express = require("express");
const DBUtil = require("../db/DBUtil");
var router = express.Router();
router.post("/fuelConsumption/:vId", async function (req, res, next) {
    let start = req.body.start;
    let end = req.body.end;
    if (end === null || end === undefined) {
        end = new Date();

        if (start === null || start === undefined) {
            let d = end.getDate() === 1 ? 28 : end.getDate() - 1;
            start = (end.getMonth() + 1) + "/" + (d-6) + "/" + end.getFullYear();
        }
        end = (end.getMonth() + 1) + "/" + end.getDate() + "/" + end.getFullYear();
    }

    let performance = await DBUtil.getPerformanceData(req.params.vId, start, end);
    let fuelData = new Map();
    for (let p of performance) {
        if (fuelData.has(p.date)) {
            let data = fuelData.get(p.date);
            if (p.name === "Starboard_FuelRate") {
                data['starboard'] = p.value;
            } else if (p.name === "Port_FuelRate") {
                data['port'] = p.value;
            }
            fuelData.set(p.date, data);
        } else {
            if (p.name === "Starboard_FuelRate") {
                fuelData.set(p.date, { starboard: p.value });
            } else if (p.name === "Port_FuelRate") {
                fuelData.set(p.date, { port: p.value });
            }
        }
    }
    //convert map to array
    let fuelDataArray = [];
    for (let [key, value] of fuelData) {
        fuelDataArray.push({ date: key, starboard: value.starboard, port: value.port, starboard_c: (value.starboard*3.247).toFixed(2), port_c: (value.port*3.247).toFixed(2) });
    }
    res.json(fuelDataArray);
});

router.post("/utilization/:vId", async function (req, res, next) {
    let start = req.body.start;
    let end = req.body.end;
    if (end === null || end === undefined) {
        end = new Date();

        if (start === null || start === undefined) {
            let d = end.getDate() === 1 ? 28 : end.getDate() - 1;
            start = (end.getMonth() + 1) + "/" + (d-6) + "/" + end.getFullYear();
        }
        end = (end.getMonth() + 1) + "/" + end.getDate() + "/" + end.getFullYear();
    }

    let performance = await DBUtil.getPerformanceData(req.params.vId, start, end);
    let utilization = new Map();
    utilization.set("Utilization_IdleCrewOff",0);
    utilization.set("Utilization_IdleCrewOn",0);
    utilization.set("Utilization_Underway",0);
    utilization.set("Utilization_Maintenance",0);

    for (let p of performance) {
        if (p.name.includes("Utilization")) {
            let total = utilization.get(p.name);
            total=total+p.value;
            utilization.set(p.name, total);
        }
    }
    //convert map to array
    let utilizationArray = [];
    for (let [key, val] of utilization) {
        utilizationArray.push({ name: key.replace("Utilization_",""), value: val });
    }
    res.json(utilizationArray);
});


router.post("/distanceTravelled/:vId", async function (req, res, next) {
    let start = req.body.start;
    let end = req.body.end;
    if (end === null || end === undefined) {
        end = new Date();

        if (start === null || start === undefined) {
            let d = end.getDate() === 1 ? 28 : end.getDate() - 1;
            start = (end.getMonth() + 1) + "/" + (d-6) + "/" + end.getFullYear();
        }
        end = (end.getMonth() + 1) + "/" + end.getDate() + "/" + end.getFullYear();
    }

    let performance = await DBUtil.getPerformanceData(req.params.vId, start, end);
    let fuelDataArray = [];
    for (let p of performance) {
        if (p.name === "Distance") {
            fuelDataArray.push({ date: p.date, distance: p.value });
        }
    }
    res.json(fuelDataArray);
});

router.post("/maintenanceActivity/:vId", async function (req, res, next) {
    let start = req.body.start;
    let end = req.body.end;
    if (end === null || end === undefined) {
        end = new Date();

        if (start === null || start === undefined) {
            let d = end.getDate() === 1 ? 28 : end.getDate() - 1;
            start = (end.getMonth() + 1) + "/" + (d-6) + "/" + end.getFullYear();
        }
        end = (end.getMonth() + 1) + "/" + end.getDate() + "/" + end.getFullYear();
    }

    let performance = await DBUtil.getPerformanceData(req.params.vId, start, end);
    let fuelData = new Map();
    for (let p of performance) {
        if (p.name.includes("Maintenance")) {
            if (fuelData.has(p.date)) {
                let data = fuelData.get(p.date);
                if (p.name === "Maintenance_Planned") {
                    if(data.planned===undefined)
                        data['planned'] = 1;
                    else
                        data['planned'] = data.planned+1;
                } else if (p.name === "Maintenance_Planned") {
                    if(data.nonPlanned===undefined)
                        data['nonPlanned'] = 1;
                    else
                        data['nonPlanned'] = data.nonPlanned+1;
                }
                fuelData.set(p.date, data);
            } else {
                if (p.name === "Maintenance_Planned") {
                    fuelData.set(p.date, { planned: 1 });
                } else if (p.name === "Maintenance_NonPlanned") {
                    fuelData.set(p.date, { nonPlanned: 1 });
                }
            }
        }
    }
    //convert map to array
    let fuelDataArray = [];
    for (let [key, value] of fuelData) {
        value.planned=value.planned===undefined?0:value.planned;
        value.nonPlanned=value.nonPlanned===undefined?0:value.nonPlanned;
        fuelDataArray.push({ date: key, planned: value.planned, nonPlanned: value.nonPlanned });
    }
    res.json(fuelDataArray);
});

module.exports = router;
/*
const dataFuelConsumption = [
    { date: "7/3", port: 200, starboard: 240 },
    { date: "7/4", port: 300, starboard: 139 },
    { date: "7/5", port: 100, starboard: 280 },
    { date: "7/6", port: 400, starboard: 190 },
  ];
 
  const dataDistanceTravelled = [
    { date: "7/3", distance: 50 },
    { date: "7/4", distance: 40 },
    { date: "7/5", distance: 70 },
    { date: "7/6", distance: 60 },
  ];
 
  const dataUtilization = [
    { name: "Maintenance", value: 10 },
    { name: "Idle-Crew-Off", value: 20 },
    { name: "Underway", value: 56 },
    { name: "Idle-Crew-On", value: 34 },
  ];
 
  const dataMaintenanceActivity = [
    { date: "7/3", planned: 2, nonPlanned: 1 },
    { date: "7/4", planned: 3, nonPlanned: 4 },
    { date: "7/5", planned: 5, nonPlanned: 2 },
    { date: "7/6", planned: 1, nonPlanned: 3 },
  ];

*/