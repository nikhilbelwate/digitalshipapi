var express = require('express');
var router = express.Router();

var MQTTConnector=require("../logic/MQTTConnector");
var SIMShip=require("../model/SIMShip");
SIMShip.init();
let mqttConnector=new MQTTConnector();

mqttConnector.subscribeTopic("dsiss/+/info",function(message){
    let data=JSON.parse(message);
    SIMShip.setShip(data);
    //console.log(data);
});

router.get("/demo",function(req,res,next){
    let s=SIMShip.getShip(1);
    s.operatingParameters.EngineSpeed.value=s.operatingParameters.EngineSpeed.value+1;
    mqttConnector.publishMessage("dsiss/3",JSON.stringify(s));
    res.json(SIMShip.getShip(1));
});
router.post("/publishMode/:vId/:control/:mode",function(req,res,next){
    let mode=req.params.mode;
    let s=SIMShip.getShip(req.params.vId);
    let control=req.params.control;
    s.controlState[control]["mode"]=mode;
    //mqttConnector.publishMessage("dsiss/"+req.params.vId,JSON.stringify(s));
    mqttConnector.publishMessage("dsiss/"+req.params.vId+"/controlState/"+control+"/mode",mode);
    res.send("Published");
});
router.post("/publishSetpoint/:vId/:control/:setpoint",function(req,res,next){
    let setpoint=req.params.setpoint;
    let s=SIMShip.getShip(req.params.vId);
    let control=req.params.control;
    s.controlState[control]["setpoint"]=setpoint;
    //mqttConnector.publishMessage("dsiss/"+req.params.vId,JSON.stringify(s));
    mqttConnector.publishMessage("dsiss/"+req.params.vId+"/controlState/"+control+"/setpoint",setpoint);
    res.send("Published");
});

router.get("/getShips",function(req,res,next){
    let data=SIMShip.getData();
    res.header('Cache-Control', 'no-cache');
    res.send(data);
});

router.get("/getShip/:vId",function(req,res,next){
    let data=SIMShip.getShip(req.params.vId);
    res.header('Cache-Control', 'no-cache');
    res.send(data);
});


module.exports = router;