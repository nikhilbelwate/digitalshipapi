const express = require('express');
var router = express.Router();
const config = require('../config.json');
const { BlobServiceClient } = require("@azure/storage-blob");

router.get('/:blobname', async function (req, res, next) {
    try {
        // Data validation
        if (req.params.blobname.length === 0) throw new Error("blob name was empty, cannot process request");
        // Download blob
        const blobServiceClient = BlobServiceClient.fromConnectionString(config.AZURE_STORAGE_CONNECTION_STRING);
        const containerClient = blobServiceClient.getContainerClient("threedobjects");
        const blockBlobClient = containerClient.getBlockBlobClient(req.params.blobname);
        const downloadBlockBlobResponse = await blockBlobClient.download(0);
        // Read the blob data from the readable stream and store it in a variable
        let blobData = '';
        for await (const chunk of downloadBlockBlobResponse.readableStreamBody) {
            blobData += chunk.toString();
        }
        res.send(blobData);
    } catch (err) {
        next(err);
    }
});
module.exports = router;
