var express = require("express");
var StatsUtil = require("../model/StatsUtil");
const ShipUtil = require("../model/ShipUtil");
const DBUtil = require("../db/DBUtil");
var router = express.Router();
router.get("/currentJobsData/:vId", async function (req, res, next) {
    let currentJobsData=await DBUtil.getCurrentJobsData(req.params.vId);
    res.json(currentJobsData);
});
router.post("/jobData/:vId", async function (req, res, next) {
    let start=req.body.start;
    let end=req.body.end;
    if(end===null || end===undefined){
      end=new Date();
      
      if(start===null || start===undefined){
        let d=end.getDate()===1?28:end.getDate()-1;
        start=(end.getMonth()+1)+"/"+d+"/"+end.getFullYear();
      }
      end=(end.getMonth()+1)+"/"+end.getDate()+"/"+end.getFullYear();
    }
    
    let jobTimeData=await DBUtil.getJobsTimelyData(req.params.vId,start,end);
    res.json(jobTimeData);
});
router.get("/weeklyJobData/:vId",async function (req, res, next) {
  
  let jobTimeData=await DBUtil.getJobsTimelyData(req.params.vId);
  res.json(jobTimeData);
})


router.post("/statusData/:vId",async function (req, res, next) {
  let start=req.body.start;
    let end=req.body.end;
    if(end===null || end===undefined){
      end=new Date();
      
      if(start===null || start===undefined){
        let d=end.getDate()===1?28:end.getDate()-1;
        start=(end.getMonth()+1)+"/"+d+"/"+end.getFullYear();
      }
      end=(end.getMonth()+1)+"/"+end.getDate()+"/"+end.getFullYear();
    }
    
  let statusTimeData=await DBUtil.getStatusTimelyData(req.params.vId,start,end);
    
  res.json(statusTimeData);
});

router.post("/scatterDataTypeCount/:vId", async function(req,res,next){
  let start=req.body.start;
    let end=req.body.end;
    if(end===null || end===undefined){
      end=new Date();
      
      if(start===null || start===undefined){
        let d=end.getDate()===1?28:end.getDate()-1;
        start=(end.getMonth()+1)+"/"+d+"/"+end.getFullYear();
      }
      end=(end.getMonth()+1)+"/"+end.getDate()+"/"+end.getFullYear();
    }
  let scatterDataByType=await DBUtil.getScatterDataByType(req.params.vId,start,end);
  
  //console.log(scatterDataByType);
  res.json(Array.from(scatterDataByType.values()));
})

router.post("/scatterData/:vId",async function (req, res, next) {
  let start=req.body.start;
    let end=req.body.end;
    if(end===null || end===undefined){
      end=new Date();
      
      if(start===null || start===undefined){
        let d=end.getDate()===1?28:end.getDate()-1;
        start=(end.getMonth()+1)+"/"+d+"/"+end.getFullYear();
      }
      end=(end.getMonth()+1)+"/"+end.getDate()+"/"+end.getFullYear();
    }
    
  let scatterData=await DBUtil.getScatterChartData(req.params.vId,start,end);
    res.json(scatterData);
});


router.post("/countsForPlottingData/:vId", async function (req, res, next) {
  let start=req.body.start;
    let end=req.body.end;
    if(end===null || end===undefined){
      end=new Date();
      if(start===null || start===undefined){
        let d=end.getDate()===1?28:end.getDate()-1;
        start=(end.getMonth()+1)+"/"+d+"/"+end.getFullYear();
      }
      end=(end.getMonth()+1)+"/"+end.getDate()+"/"+end.getFullYear();
    }
    let countResult=await DBUtil.getCountForPlottingData(req.params.vId,start,end);
    res.json(countResult);
  });


router.post("/timePlottingData/:vId", async function (req, res, next) {
  let start=req.body.start;
    let end=req.body.end;
    if(end===null || end===undefined){
      end=new Date();
      
      if(start===null || start===undefined){
        let d=end.getDate()===1?28:end.getDate()-1;
        start=(end.getMonth()+1)+"/"+d+"/"+end.getFullYear();
      }
      end=(end.getMonth()+1)+"/"+end.getDate()+"/"+end.getFullYear();
    }
  
  timePlottingData=await DBUtil.getTimePlottingData(req.params.vId,start,end);
  for(let d of timePlottingData){
    delete d.yearlyData;
  }
  res.json(timePlottingData);
});


module.exports = router;
  