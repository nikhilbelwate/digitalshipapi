var express = require('express');
var router = express.Router();
var ShipUtil = require('../model/ShipUtil');

router.get('/', function (req, res, next) {

    res.json(ShipUtil.getShips());
});

router.get('/:sId', function (req, res, next) {

    res.json(ShipUtil.getShip(req.params.sId));
});

router.get('/stat', function (req, res,next) {
    stat = {

        "ships": {

            "234123": {
                "vessel": "Seaspan Raven",
                "vesselMaintenance": {
                    "PMJobs": 6,
                    "NonPMJob": 6,
                    "WorkOrder": 6,
                    "certNext3M": 2,
                    "certNext6M": 4,
                    "certNext9M": 3,
                    "standardJob": 2,
                    "workOrderByCrew": 6,
                    "standardOperation": 3,
                    "standardRequistion": 3,
                },
                "vesselOperationStatus": {
                    "underway": 400,
                    "maintenance": 10,
                    "idleCrewOn": 11.2,
                    "idleCrewOff": 300,

                }

            },
            "567987": {
                "vessel": "Seaspan Eagle",
                "vesselMaintenance": {
                    "PMJobs": 4,
                    "NonPMJob": 4,
                    "WorkOrder": 4,
                    "certNext3M": 4,
                    "certNext6M": 6,
                    "certNext9M": 6,
                    "standardJob": 4,
                    "workOrderByCrew": 4,
                    "standardOperation": 3,
                    "standardRequistion": 7,
                },
                "vesselOperationStatus": {
                    "underway": 300,
                    "maintenance": 100,
                    "idleCrewOn": 11.2,
                    "idleCrewOff": 300,

                }

            }
        }
    }
    res.json(stat);
});

module.exports = router;