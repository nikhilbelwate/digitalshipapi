var express = require("express");
const ShipUtil = require("../model/ShipUtil");
const DBUtil = require("../db/DBUtil");
const { replacer } = require("../model/AlertUtil");
var router = express.Router();
router.post("/data/:vId", async function (req, res, next) {
    let start=req.body.start;
    let end=req.body.end;
    if(end===null || end===undefined){
      end=new Date();
      
      if(start===null || start===undefined){
        let d=end.getDate()===1?28:end.getDate()-1;
        start=(end.getMonth()+1)+"/"+d+"/"+end.getFullYear();
      }
      end=(end.getMonth()+1)+"/"+end.getDate()+"/"+end.getFullYear();
    }
    
    let result=await DBUtil.getMonitoringData(req.params.vId,start,end);
    res.json(result);
});

router.post("/data_map/:vId", async function (req, res, next) {
    let start=req.body.start;
    let end=req.body.end;
    if(end===null || end===undefined){
      end=new Date();
      
      if(start===null || start===undefined){
        let d=end.getDate()===1?28:end.getDate()-1;
        start=(end.getMonth()+1)+"/"+d+"/"+end.getFullYear();
      }
      end=(end.getMonth()+1)+"/"+end.getDate()+"/"+end.getFullYear();
    }
    
    let result=await DBUtil.getMonitoringData(req.params.vId,start,end);
    let data=new Map();
    for(let row of result){
        if(data.has(row.engine)){
            let engine=data.get(row.engine)
            if(engine instanceof Map){
                if(engine.has(row.graph)){
                    let graph=engine.get(row.graph);
                        if(graph.has(row.param)){
                            let param=graph.get(row.param);
                                param.push(row);                            
                        }else{
                            let records=new Array();
                            records.push(row);
                            graph.set(row.param,records)
                        }
                    
                }else{
                    let records=new Array();
                    records.push(row);
                    let param=new Map();
                    param.set(row.param,records);
                    engine.set(row.graph,param);
                }
            }
        }else{
            let records=new Array();
            records.push(row);
            let param=new Map();
            param.set(row.param,records);
            let graph=new Map();
            graph.set(row.graph,param);
            data.set(row.engine, graph);
        }
    }
    
    console.log(data);
    res.send(JSON.stringify(data,replacer));
});
  
module.exports = router;