let sent={
    "vessel": {
      "name": "Seaspan Raven",
      "id": 5
    },
    "vesselStatus": {
      "status": "Underway",
      "task": "Barge Escort",
      "lastUpdate": "1681589219578"
    },
    "currentPosition": {
      "gps": {
        "displayName": "Location",
        "latitude": {
          "displayName": "Latitude",
          "value": 49.2917442,
          "unit": "N"
        },
        "longitude": {
          "displayName": "Longitude",
          "value": -125.03479,
          "unit": "W"
        }
      },
      "speedOverGround": {
        "displayName": "Speed Over Ground",
        "value": 0.91,
        "unit": "knots"
      },
      "courseOverGround": {
        "displayName": "Course Over Ground",
        "value": 6.0408,
        "unit": "°"
      },
      "lastUpdate": "1690589930"
    },
    "computedParameters": {
      "engineRPM": {
        "displayName": "Engine Speed",
        "value": 1115.06,
        "units": "rpm",
        "columnName": [
          "Port_MainEngine_EngineSpeed",
          "Starboard_MainEngine_EngineSpeed"
        ],
        "type": "average"
      },
      "engineLoad": {
        "displayName": "Engine Load",
        "columnName": [
          "Port_MainEngine_PercentLoadAtCurrentSpeed",
          "Starboard_MainEngine_PercentLoadAtCurrentSpeed"
        ],
        "value": 33.5,
        "units": "%",
        "type": "average"
      },
      "fuelBurn": {
        "displayName": "Fuel Burn",
        "columnName": [
          "Port_MainEngine_FuelRate",
          "Starboard_MainEngine_FuelRate"
        ],
        "value": 76.92500000000001,
        "units": "L/h",
        "type": "average"
      }
    },
    "operatingParameters": {
      "engineRPM": {
        "displayName": "Engine Speed",
        "unit": "rpm",
        "values": [
          {
            "name": "Port",
            "value": 2000.62
          },
          {
            "name": "Starboard",
            "value": 1040.5
          }
        ]
      },
      "engineLoad": {
        "displayName": "Engine Load",
        "unit": "%",
        "values": [
          {
            "name": "Port",
            "value": 34
          },
          {
            "name": "Starboard",
            "value": 33
          }
        ]
      },
      "fuelBurn": {
        "displayName": "Fuel Burn",
        "unit": "L/h",
        "values": [
          {
            "name": "Port",
            "value": 79.4
          },
          {
            "name": "Starboard",
            "value": 74.45
          }
        ]
      },
      "oilTemperature": {
        "displayName": "Oil Temperature",
        "unit": "C",
        "values": [
          {
            "name": "Port",
            "value": 93.3125
          },
          {
            "name": "Starboard",
            "value": 125
          }
        ]
      },
      "oilPressure": {
        "displayName": "Oil Pressure",
        "unit": "kPa",
        "values": [
          {
            "name": "Port",
            "value": 384
          },
          {
            "name": "Starboard",
            "value": 352
          }
        ]
      }
    },
    "history": {
      "engineRPM": {
        "displayName": "Engine Speed",
        "units": "rpm",
        "value": [
          {
            "timeStamp": "1690588800",
            "value": "1323.5"
          },
          {
            "timeStamp": "1690587000",
            "value": "1295.1"
          },
          {
            "timeStamp": "1690585200",
            "value": "1147.29"
          },
          {
            "timeStamp": "1690583400",
            "value": "409.89"
          },
          {
            "timeStamp": "1690581600",
            "value": "761.21"
          },
          {
            "timeStamp": "1690579800",
            "value": "0.0"
          },
          {
            "timeStamp": "1690578000",
            "value": "0.0"
          },
          {
            "timeStamp": "1690576200",
            "value": "0.0"
          },
          {
            "timeStamp": "1690574400",
            "value": "0.0"
          },
          {
            "timeStamp": "1690572600",
            "value": "0.0"
          },
          {
            "timeStamp": "1690570800",
            "value": "0.0"
          },
          {
            "timeStamp": "1690569000",
            "value": "0.0"
          },
          {
            "timeStamp": "1690567200",
            "value": "0.0"
          },
          {
            "timeStamp": "1690565400",
            "value": "0.0"
          },
          {
            "timeStamp": "1690563600",
            "value": "19.38"
          },
          {
            "timeStamp": "1690561800",
            "value": "879.25"
          },
          {
            "timeStamp": "1690560000",
            "value": "1213.09"
          },
          {
            "timeStamp": "1690558200",
            "value": "1321.67"
          },
          {
            "timeStamp": "1690556400",
            "value": "1151.88"
          },
          {
            "timeStamp": "1690554600",
            "value": "1122.8"
          },
          {
            "timeStamp": "1690552800",
            "value": "1152.72"
          },
          {
            "timeStamp": "1690551000",
            "value": "980.46"
          },
          {
            "timeStamp": "1690549200",
            "value": "813.3"
          },
          {
            "timeStamp": "1690547400",
            "value": "967.91"
          },
          {
            "timeStamp": "1690545600",
            "value": "970.46"
          },
          {
            "timeStamp": "1690543800",
            "value": "518.26"
          },
          {
            "timeStamp": "1690542000",
            "value": "0.0"
          },
          {
            "timeStamp": "1690540200",
            "value": "79.56"
          },
          {
            "timeStamp": "1690538400",
            "value": "989.83"
          },
          {
            "timeStamp": "1690536600",
            "value": "941.89"
          },
          {
            "timeStamp": "1690534800",
            "value": "820.14"
          },
          {
            "timeStamp": "1690533000",
            "value": "992.22"
          },
          {
            "timeStamp": "1690531200",
            "value": "1117.27"
          },
          {
            "timeStamp": "1690529400",
            "value": "616.78"
          },
          {
            "timeStamp": "1690527600",
            "value": "358.92"
          },
          {
            "timeStamp": "1690525800",
            "value": "978.58"
          },
          {
            "timeStamp": "1690524000",
            "value": "536.76"
          },
          {
            "timeStamp": "1690522200",
            "value": "0.0"
          },
          {
            "timeStamp": "1690520400",
            "value": "0.0"
          },
          {
            "timeStamp": "1690518600",
            "value": "0.0"
          },
          {
            "timeStamp": "1690516800",
            "value": "0.0"
          },
          {
            "timeStamp": "1690515000",
            "value": "0.0"
          },
          {
            "timeStamp": "1690513200",
            "value": "0.0"
          },
          {
            "timeStamp": "1690511400",
            "value": "0.0"
          },
          {
            "timeStamp": "1690509600",
            "value": "0.0"
          },
          {
            "timeStamp": "1690507800",
            "value": "0.0"
          },
          {
            "timeStamp": "1690506000",
            "value": "17.7"
          },
          {
            "timeStamp": "1690504200",
            "value": "999.58"
          },
          {
            "timeStamp": "1690502400",
            "value": "1251.0"
          }
        ]
      },
      "engineLoad": {
        "displayName": "Engine Load",
        "columnName": [
          "Port_MainEngine_PercentLoadAtCurrentSpeed",
          "Starboard_MainEngine_PercentLoadAtCurrentSpeed"
        ],
        "units": "%",
        "value": [
          {
            "timeStamp": "1690588800",
            "value": "42.02"
          },
          {
            "timeStamp": "1690587000",
            "value": "36.57"
          },
          {
            "timeStamp": "1690585200",
            "value": "24.64"
          },
          {
            "timeStamp": "1690583400",
            "value": "3.79"
          },
          {
            "timeStamp": "1690581600",
            "value": "8.95"
          },
          {
            "timeStamp": "1690579800",
            "value": "0.0"
          },
          {
            "timeStamp": "1690578000",
            "value": "0.0"
          },
          {
            "timeStamp": "1690576200",
            "value": "0.0"
          },
          {
            "timeStamp": "1690574400",
            "value": "0.0"
          },
          {
            "timeStamp": "1690572600",
            "value": "0.0"
          },
          {
            "timeStamp": "1690570800",
            "value": "0.0"
          },
          {
            "timeStamp": "1690569000",
            "value": "0.0"
          },
          {
            "timeStamp": "1690567200",
            "value": "0.0"
          },
          {
            "timeStamp": "1690565400",
            "value": "0.0"
          },
          {
            "timeStamp": "1690563600",
            "value": "0.0"
          },
          {
            "timeStamp": "1690561800",
            "value": "8.88"
          },
          {
            "timeStamp": "1690560000",
            "value": "33.25"
          },
          {
            "timeStamp": "1690558200",
            "value": "42.72"
          },
          {
            "timeStamp": "1690556400",
            "value": "29.04"
          },
          {
            "timeStamp": "1690554600",
            "value": "28.85"
          },
          {
            "timeStamp": "1690552800",
            "value": "32.84"
          },
          {
            "timeStamp": "1690551000",
            "value": "23.97"
          },
          {
            "timeStamp": "1690549200",
            "value": "6.0"
          },
          {
            "timeStamp": "1690547400",
            "value": "14.25"
          },
          {
            "timeStamp": "1690545600",
            "value": "12.08"
          },
          {
            "timeStamp": "1690543800",
            "value": "4.48"
          },
          {
            "timeStamp": "1690542000",
            "value": "0.0"
          },
          {
            "timeStamp": "1690540200",
            "value": "0.0"
          },
          {
            "timeStamp": "1690538400",
            "value": "15.26"
          },
          {
            "timeStamp": "1690536600",
            "value": "8.62"
          },
          {
            "timeStamp": "1690534800",
            "value": "5.27"
          },
          {
            "timeStamp": "1690533000",
            "value": "14.84"
          },
          {
            "timeStamp": "1690531200",
            "value": "23.93"
          },
          {
            "timeStamp": "1690529400",
            "value": "9.74"
          },
          {
            "timeStamp": "1690527600",
            "value": "2.06"
          },
          {
            "timeStamp": "1690525800",
            "value": "12.76"
          },
          {
            "timeStamp": "1690524000",
            "value": "5.46"
          },
          {
            "timeStamp": "1690522200",
            "value": "0.0"
          },
          {
            "timeStamp": "1690520400",
            "value": "0.0"
          },
          {
            "timeStamp": "1690518600",
            "value": "0.0"
          },
          {
            "timeStamp": "1690516800",
            "value": "0.0"
          },
          {
            "timeStamp": "1690515000",
            "value": "0.0"
          },
          {
            "timeStamp": "1690513200",
            "value": "0.0"
          },
          {
            "timeStamp": "1690511400",
            "value": "0.0"
          },
          {
            "timeStamp": "1690509600",
            "value": "0.0"
          },
          {
            "timeStamp": "1690507800",
            "value": "0.0"
          },
          {
            "timeStamp": "1690506000",
            "value": "0.0"
          },
          {
            "timeStamp": "1690504200",
            "value": "25.29"
          },
          {
            "timeStamp": "1690502400",
            "value": "55.46"
          }
        ]
      },
      "fuelBurn": {
        "displayName": "Fuel Burn",
        "columnName": [
          "Port_MainEngine_FuelRate",
          "Starboard_MainEngine_FuelRate"
        ],
        "units": "L/h",
        "value": [
          {
            "timeStamp": "1690588800",
            "value": "132.34"
          },
          {
            "timeStamp": "1690587000",
            "value": "141.59"
          },
          {
            "timeStamp": "1690585200",
            "value": "66.34"
          },
          {
            "timeStamp": "1690583400",
            "value": "19.95"
          },
          {
            "timeStamp": "1690581600",
            "value": "42.01"
          },
          {
            "timeStamp": "1690579800",
            "value": "0.0"
          },
          {
            "timeStamp": "1690578000",
            "value": "0.0"
          },
          {
            "timeStamp": "1690576200",
            "value": "0.0"
          },
          {
            "timeStamp": "1690574400",
            "value": "0.0"
          },
          {
            "timeStamp": "1690572600",
            "value": "0.0"
          },
          {
            "timeStamp": "1690570800",
            "value": "0.0"
          },
          {
            "timeStamp": "1690569000",
            "value": "0.0"
          },
          {
            "timeStamp": "1690567200",
            "value": "0.0"
          },
          {
            "timeStamp": "1690565400",
            "value": "0.0"
          },
          {
            "timeStamp": "1690563600",
            "value": "1.28"
          },
          {
            "timeStamp": "1690561800",
            "value": "44.68"
          },
          {
            "timeStamp": "1690560000",
            "value": "91.03"
          },
          {
            "timeStamp": "1690558200",
            "value": "127.6"
          },
          {
            "timeStamp": "1690556400",
            "value": "84.78"
          },
          {
            "timeStamp": "1690554600",
            "value": "102.96"
          },
          {
            "timeStamp": "1690552800",
            "value": "133.75"
          },
          {
            "timeStamp": "1690551000",
            "value": "136.62"
          },
          {
            "timeStamp": "1690549200",
            "value": "44.86"
          },
          {
            "timeStamp": "1690547400",
            "value": "54.82"
          },
          {
            "timeStamp": "1690545600",
            "value": "47.58"
          },
          {
            "timeStamp": "1690543800",
            "value": "25.67"
          },
          {
            "timeStamp": "1690542000",
            "value": "0.0"
          },
          {
            "timeStamp": "1690540200",
            "value": "5.42"
          },
          {
            "timeStamp": "1690538400",
            "value": "52.67"
          },
          {
            "timeStamp": "1690536600",
            "value": "44.28"
          },
          {
            "timeStamp": "1690534800",
            "value": "43.36"
          },
          {
            "timeStamp": "1690533000",
            "value": "57.24"
          },
          {
            "timeStamp": "1690531200",
            "value": "85.25"
          },
          {
            "timeStamp": "1690529400",
            "value": "40.32"
          },
          {
            "timeStamp": "1690527600",
            "value": "20.23"
          },
          {
            "timeStamp": "1690525800",
            "value": "47.42"
          },
          {
            "timeStamp": "1690524000",
            "value": "26.85"
          },
          {
            "timeStamp": "1690522200",
            "value": "0.0"
          },
          {
            "timeStamp": "1690520400",
            "value": "0.0"
          },
          {
            "timeStamp": "1690518600",
            "value": "0.0"
          },
          {
            "timeStamp": "1690516800",
            "value": "0.0"
          },
          {
            "timeStamp": "1690515000",
            "value": "0.0"
          },
          {
            "timeStamp": "1690513200",
            "value": "0.0"
          },
          {
            "timeStamp": "1690511400",
            "value": "0.0"
          },
          {
            "timeStamp": "1690509600",
            "value": "0.0"
          },
          {
            "timeStamp": "1690507800",
            "value": "0.0"
          },
          {
            "timeStamp": "1690506000",
            "value": "1.17"
          },
          {
            "timeStamp": "1690504200",
            "value": "83.13"
          },
          {
            "timeStamp": "1690502400",
            "value": "179.15"
          }
        ]
      },
      "status": {
        "displayName": "Status History",
        "units": "min",
        "value": [
          {
            "status": "Underway",
            "startTime": "1681589219568",
            "endTime": "1681589219568",
            "duration": 200
          },
          {
            "status": "Maintenance",
            "startTime": "1681589219569",
            "endTime": "1681589219569",
            "duration": 42
          },
          {
            "status": "Underway",
            "timeStamp": "1681589219570",
            "duration": 400
          },
          {
            "status": "Idle - Crew On",
            "startTime": "1681589219569",
            "endTime": "1681589219569",
            "duration": 20
          },
          {
            "status": "Idle - Crew Off",
            "startTime": "1681589219569",
            "endTime": "1681589219569",
            "duration": 100
          },
          {
            "status": "Underway",
            "startTime": "1681589219569",
            "endTime": "1681589219569",
            "duration": 500
          },
          {
            "status": "Maintenance",
            "startTime": "1681589219569",
            "endTime": "1681589219569",
            "duration": 178
          }
        ]
      }
    },
    "vesselLastWeek": {
      "fuelBurn": {
        "displayName": "Fuel Burn",
        "columnName": [
          "Port_MainEngine_FuelRate",
          "Starboard_MainEngine_FuelRate"
        ],
        "value": 3034,
        "unit": "L"
      },
      "distance": {
        "displayName": "Total Distance",
        "value": 1223,
        "unit": "km"
      },
      "timeUnderway": {
        "displayName": "Total Time Underway",
        "value": 720.098098,
        "unit": "min"
      },
      "timeUnderLoad": {
        "displayName": "Total Time Under Load",
        "value": 800.454,
        "unit": "min"
      }
    },
    "vesselTotal": {
      "fuelBurn": {
        "displayName": "Total Fuel Burn",
        "columnName": [
          "Port_MainEngine_FuelRate",
          "Starboard_MainEngine_FuelRate"
        ],
        "value": 80000,
        "unit": "L"
      },
      "distance": {
        "displayName": "Total Distance",
        "value": 40002,
        "unit": "km"
      },
      "engineHours": {
        "displayName": "Total Engine Hours",
        "value": 40000,
        "unit": "h"
      },
      "operatingCost": {
        "displayName": "Operating Cost",
        "value": 15.5,
        "unit": "$/h.hp"
      }
    }
  };

let original={
    "vessel": {
        "name": "Seaspan Eagle",
        "engineType": "C181",
        "gensetType": "ER-234#45",
        "id": 4
    },
    "vesselStatus": {
        "status": "Underway",
        "task": "Barge Escort",
        "destination": "Seaspan",
        "eta": "1.34",
        "lastUpdate": "1681589219578"
    },
    "alert": [
        {}
    ],
    "currentValues": {
        "latitude": 49.3529,
        "longitude": -123.91968,
        "course": 232.34,
        "speedOverGround": 14.45535453,
        "engineRPM": 1001,
        "engineLoad": 70.689898,
        "auxGenPower": 30.343444444444444,
        "fuelBurn": 67.232,
        "lastUpdate": "1681589219578"
    },
    "engineSpeedHistory": [
        {
            "timeStamp": "1681589219551",
            "value": "45"
        },
        {
            "timeStamp": "1681589219552",
            "value": "47"
        },
        {
            "timeStamp": "1681589219553",
            "value": "48"
        },
        {
            "timeStamp": "1681589219554",
            "value": "49"
        },
        {
            "timeStamp": "1681589219556",
            "value": "48"
        },
        {
            "timeStamp": "1681589219560",
            "value": "49"
        },
        {
            "timeStamp": "1681589219563",
            "value": "52"
        },
        {
            "timeStamp": "1681589219564",
            "value": "50"
        },
        {
            "timeStamp": "1681589219566",
            "value": "52"
        },
        {
            "timeStamp": "1681589219568",
            "value": "53"
        }
    ],
    "engineLoadHistory": [
        {
            "timeStamp": "1681589219551",
            "value": "55"
        },
        {
            "timeStamp": "1681589219552",
            "value": "67"
        },
        {
            "timeStamp": "1681589219553",
            "value": "42"
        },
        {
            "timeStamp": "1681589219554",
            "value": "49"
        },
        {
            "timeStamp": "1681589219556",
            "value": "48"
        },
        {
            "timeStamp": "1681589219560",
            "value": "49"
        },
        {
            "timeStamp": "1681589219563",
            "value": "52"
        },
        {
            "timeStamp": "1681589219564",
            "value": "50"
        },
        {
            "timeStamp": "1681589219566",
            "value": "52"
        },
        {
            "timeStamp": "1681589219568",
            "value": "53"
        }
    ],
    "fuelBurnHistory": [
        {
            "timeStamp": "1681589219551",
            "value": "55"
        },
        {
            "timeStamp": "1681589219552",
            "value": "67"
        },
        {
            "timeStamp": "1681589219553",
            "value": "42"
        },
        {
            "timeStamp": "1681589219554",
            "value": "49"
        },
        {
            "timeStamp": "1681589219556",
            "value": "48"
        },
        {
            "timeStamp": "1681589219560",
            "value": "49"
        },
        {
            "timeStamp": "1681589219563",
            "value": "52"
        },
        {
            "timeStamp": "1681589219564",
            "value": "50"
        },
        {
            "timeStamp": "1681589219566",
            "value": "52"
        },
        {
            "timeStamp": "1681589219568",
            "value": "53"
        }
    ],
    "auxGenPowerHistory": [
        {
            "timeStamp": "1681589219551",
            "value": "55"
        },
        {
            "timeStamp": "1681589219552",
            "value": "67"
        },
        {
            "timeStamp": "1681589219553",
            "value": "42"
        },
        {
            "timeStamp": "1681589219554",
            "value": "49"
        },
        {
            "timeStamp": "1681589219556",
            "value": "48"
        },
        {
            "timeStamp": "1681589219560",
            "value": "49"
        },
        {
            "timeStamp": "1681589219563",
            "value": "52"
        },
        {
            "timeStamp": "1681589219564",
            "value": "50"
        },
        {
            "timeStamp": "1681589219566",
            "value": "52"
        },
        {
            "timeStamp": "1681589219568",
            "value": "53"
        }
    ],
    "statusHistory": [
        {
            "status": "Underway",
            "timeStamp": "1681589219568",
            "duration": 200
        },
        {
            "status": "Maintenance",
            "timeStamp": "1681589219569",
            "duration": 42
        },
        {
            "status": "Underway",
            "timeStamp": "1681589219570",
            "duration": 400
        },
        {
            "status": "Idle - Crew On",
            "timeStamp": "1681589219570",
            "duration": 20
        },
        {
            "status": "Idle - Crew Off",
            "timeStamp": "1681589219571",
            "duration": 100
        },
        {
            "status": "Underway",
            "timeStamp": "1681589219572",
            "duration": 500
        },
        {
            "status": "Maintenance",
            "timeStamp": "1681589219573",
            "duration": 178
        }
    ],
    "currentPosition": {
        "gps": {
            "displayName": "Location",
            "color": "#C8102E",
            "latitude": {
                "displayName": "Latitude",
                "displayValue": "49° 21' 10\"",
                "value": 49.3529,
                "unit": "N"
            },
            "longitude": {
                "displayName": "Longitude",
                "displayValue": "123° 55' 11\"",
                "value": -123.91968,
                "unit": "W"
            }
        },
        "speedOverGround": {
            "displayName": "Speed Over Ground",
            "displayValue": "3.28",
            "value": 3.28,
            "unit": "knots"
        },
        "heading": {
            "displayName": "Heading",
            "displayValue": "2.3",
            "value": 2.3,
            "unit": "°"
        },
        "courseOverGround": {
            "displayName": "Course Over Ground",
            "displayValue": "232° 20' 24\"",
            "value": 232.34,
            "unit": "°"
        }
    },
    "operatingParameters": {
        "engineRPM": {
            "displayName": "Engine Speed",
            "unit": "rpm",
            "values": [
                {
                    "name": "Port",
                    "value": 1220,
                    "maxValue": 1500,
                    "color": "#001A71"
                },
                {
                    "name": "Starboard",
                    "value": 1122,
                    "maxValue": 1500,
                    "color": "#001A71"
                }
            ]
        }
    },
    "engineLoad": {
        "displayName": "Engine Load",
        "unit": "%",
        "values": [
            {
                "name": "Port",
                "value": 90,
                "maxValue": 100,
                "color": "#C8102E"
            },
            {
                "name": "Starboard",
                "value": 70,
                "maxValue": 100,
                "color": "#001A71"
            }
        ]
    },
    "fuelBurn": {
        "displayName": "Fuel Burn",
        "unit": "L/h",
        "values": [
            {
                "name": "Port",
                "value": 500,
                "maxValue": 550,
                "color": "#C8102E"
            },
            {
                "name": "Starboard",
                "value": 440,
                "maxValue": 550,
                "color": "#001A71"
            }
        ]
    },
    "generator": {
        "displayName": "Generator",
        "unit": "kW",
        "values": [
            {
                "name": "Port",
                "value": 0,
                "maxValue": 350,
                "color": "#969696"
            },
            {
                "name": "Starboard",
                "value": 300,
                "maxValue": 350,
                "color": "#001A71"
            }
        ]
    },
    "oilTemperature": {
        "displayName": "Oil Temperature",
        "unit": "C",
        "values": [
            {
                "name": "Port",
                "value": 120,
                "maxValue": 200,
                "color": "#001A71"
            },
            {
                "name": "Starboard",
                "value": 112,
                "maxValue": 200,
                "color": "#001A71"
            }
        ]
    },
    "oilPressure": {
        "displayName": "Oil Pressure",
        "unit": "kPa",
        "values": [
            {
                "name": "Port",
                "value": 1400,
                "maxValue": 1500,
                "color": "#001A71"
            },
            {
                "name": "Starboard",
                "value": 1390,
                "maxValue": 1500,
                "color": "#C8102E"
            }
        ]
    },
    "vesselLastWeek": {
        "fuelBurn": {
            "displayName": "Fuel Burn",
            "displayValue": "3,034",
            "value": 3034,
            "unit": "L"
        },
        "totalDistance": {
            "displayName": "Total Distance",
            "displayValue": "1,223",
            "value": 1223,
            "unit": "km"
        },
        "totalTimeUnderway": {
            "displayName": "Total Time Underway",
            "displayValue": "720",
            "value": 720.098098,
            "unit": "min"
        },
        "totalTimeUnderLoad": {
            "displayName": "Total Time Under Load",
            "displayValue": "800",
            "value": 800.454,
            "unit": "min"
        }
    },
    "vesselTotal": {
        "totalFuelBurn": {
            "displayName": "Total Fuel Burn",
            "displayValue": "80,000",
            "value": 80000,
            "unit": "L"
        },
        "totalDistance": {
            "displayName": "Total Distance",
            "displayValue": "40,002",
            "value": 40002,
            "unit": "km"
        },
        "totalEngineHours": {
            "displayName": "Total Engine Hours",
            "displayValue": "40,000",
            "value": 40000,
            "unit": "h"
        },
        "operatingCost": {
            "displayName": "Operating Cost",
            "displayValue": "15.50",
            "value": 15.5,
            "unit": "$/h.hp"
        }
    }
};

    function mergeObjects(obj1, obj2) {
        const merged = Object.assign({}, obj1);
        console.log("obj1="+JSON.stringify(obj1));
        for (const key in obj2) {
            if (obj2.hasOwnProperty(key)) {
                if (typeof obj2[key] === "object" && obj2[key] !== null && !Array.isArray(obj2[key])) {
                    if (typeof obj1[key] === "object" && obj1[key] !== null && !Array.isArray(obj1[key])) {
                        merged[key] = mergeObjects(obj1[key], obj2[key]);
                    } else {
                        merged[key] = Object.assign({}, obj2[key]);
                    }
                } else {
                    merged[key] = obj2[key];
                }
            }
        }
        
        return merged;
    }
    console.log(JSON.stringify(mergeObjects(original,sent)));