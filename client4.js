var data={

    "ships": {

        "5": {
            "vessel": "Seaspan Raven",
            "vesselMaintenance": {
                "PMJobs": 9,
                "NonPMJob": 6,
                "WorkOrder": 6,
                "certNext3M": 2,
                "certNext6M": 4,
                "certNext9M": 3,
                "standardJob": 2,
                "workOrderByCrew": 6,
                "standardOperation": 6,
                "standardRequistion": 3
            },
            "vesselOperationStatus": {
                "underway": 400,
                "maintenance": 10,
                "idleCrewOn": 11.2,
                "idleCrewOff": 300

            }

        },
        "4": {
            "vessel": "Seaspan Eagle",
            "vesselMaintenance": {
                "PMJobs": 4,
                "NonPMJob": 4,
                "WorkOrder": 4,
                "certNext3M": 4,
                "certNext6M": 6,
                "certNext9M": 6,
                "standardJob": 4,
                "workOrderByCrew": 4,
                "standardOperation": 3,
                "standardRequistion": 7
            },
            "vesselOperationStatus": {
                "underway": 300,
                "maintenance": 100,
                "idleCrewOn": 11.2,
                "idleCrewOff": 300
            }

        }
    }
};
class ShipUtil{
    static sumOfObjects(ship1, ship2) {
        const sumObj = Object.assign({}, ship1);
        let obj1=ship1.vesselMaintenance;
        let obj2=ship2.vesselMaintenance;
        for (const key in obj2) {
          if (obj2.hasOwnProperty(key)) {
            
                sumObj.vesselMaintenance[key] = parseFloat(obj1[key])+parseFloat(obj2[key]);
          }
        }
        obj1={...ship1.vesselOperationStatus};
        obj2={...ship2.vesselOperationStatus};
        for (const key in obj2) {
          if (obj2.hasOwnProperty(key)) {
            
                sumObj.vesselOperationStatus[key] = obj1[key]+obj2[key];
          }
        }
        return sumObj;
      }
}
let allShips=null;
Object.entries(data.ships).map(([k, s]) => {
    console.log("s",s);
    if (allShips == null) {
      allShips = {...s};
    } else {
      allShips=ShipUtil.sumOfObjects(JSON.parse(JSON.stringify(allShips)) ,s);
    }
    //return allShips
  });
  console.log("allShips="+JSON.stringify(allShips));
    
  let piaArray = [];
  let info = {};
  info["name"] = "Certificates Due By Ship";
  info["id"] = 1;
  info["data"] = [
    { name: "Next 3 Months", value: allShips.vesselMaintenance.certNext3M, ships:[] },
    { name: "Next 6 Months", value: allShips.vesselMaintenance.certNext6M, ships:[] },
    { name: "Next 9 Months", value: allShips.vesselMaintenance.certNext9M, ships:[] },
  ];
     
  piaArray[0] = info;
  info = {};
  info["name"] = "Open Work Orders";
  info["id"] = 2;
  info["data"] = [
    {
      name: "Preventive Maintenance",
      value: allShips.vesselMaintenance.PMJobs,
      ships:[]
    },
    {
      name: "Non-Preventive Maintenance",
      value: allShips.vesselMaintenance.NonPMJob,
      ships:[]
    },
  ];
  
  piaArray[1] = info;
  info = {};
  info["name"] = "Overdue Maintenance Jobs";
  info["id"] = 3;
  info["data"] = [
    { name: "Standard Job", value: allShips.vesselMaintenance.standardJob,ships:[] },
    {
      name: "Work Order by Crew",
      value: allShips.vesselMaintenance.workOrderByCrew,
      ships:[]
    },
    {
      name: "Standard Operation",
      value: allShips.vesselMaintenance.standardOperation,
      ships:[]
    },
    {
      name: "Standard Requisition",
      value: allShips.vesselMaintenance.standardRequistion,
      ships:[]
    },
  ];
  piaArray[2] = info;
  info = {};
  info["name"] = "Fleet Status Last Month";
  info["id"] = 4;
  info["data"] = [
    { name: "Underway", value: 55, ships:[{shipname:"Seaspan Raven", value:10},{shipname:"Seaspan Eagle", value:8}] },
    { name: "Idle - Crew On", value: 15, ships:[{shipname:"Seaspan Raven", value:12},{shipname:"Seaspan Eagle", value:9}] },
    { name: "Maintenance", value: 20, ships:[{shipname:"Seaspan Raven", value:8},{shipname:"Seaspan Eagle", value:8}] },
    { name: "Idle - Crew Off", value: 10, ships:[{shipname:"Seaspan Raven", value:4},{shipname:"Seaspan Eagle", value:3}] },
  ];
  piaArray[3] = info;
  Object.entries(data.ships).map(([k, s]) => {
      piaArray[0].data[0].ships.push({shipname:s.vessel, value:s.vesselMaintenance.certNext3M});
      piaArray[0].data[1].ships.push({shipname:s.vessel, value:s.vesselMaintenance.certNext6M});
      piaArray[0].data[2].ships.push({shipname:s.vessel, value:s.vesselMaintenance.certNext9M});
      piaArray[1].data[0].ships.push({shipname:s.vessel, value:s.vesselMaintenance.PMJobs});
      piaArray[1].data[1].ships.push({shipname:s.vessel, value:s.vesselMaintenance.NonPMJob});
      piaArray[2].data[0].ships.push({shipname:s.vessel, value:s.vesselMaintenance.standardJob});
      piaArray[2].data[1].ships.push({shipname:s.vessel, value:s.vesselMaintenance.workOrderByCrew});
      piaArray[2].data[2].ships.push({shipname:s.vessel, value:s.vesselMaintenance.standardOperation});
      piaArray[2].data[3].ships.push({shipname:s.vessel, value:s.vesselMaintenance.standardRequistion});
  });
  
  console.log(JSON.stringify(piaArray));