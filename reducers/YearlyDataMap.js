class YearlyDataMap{
    
    constructor(row){
        this.yearMap=new Map();
        this.item=row?.job_name;
        this.addRecord(row);
    }

    addRecord=(row)=>{
        //console.log(row?.date_scheduled?.getFullYear() + "->"+row?.status);
        let year =row?.date_scheduled?.getFullYear();
        let status=row?.status;
        if(status.includes("DUE SOON")){
            this.yearMap.set(year,{duesoon:this.getDayOfYear(row?.date_scheduled), date:row?.date_scheduled});
        }else if(status.includes("SCHEDULED")){
            this.yearMap.set(year,{scheduled:this.getDayOfYear(row?.date_scheduled)});
        }else{
            if(row?.date_completed!==null && row?.date_completed!==undefined)
                this.yearMap.set(year,{completed:this.getDayOfYear(row?.date_completed)});
            else
                this.yearMap.set(year,{overdue:this.getDayOfYear(row?.date_scheduled)});
        }
    }
    getYearMap=()=>{
        return Object.fromEntries(this.yearMap.entries());
    }
    getDayOfYear=(dateString)=> {
        const date = new Date(dateString);
        if (isNaN(date)) {
          throw new Error("Invalid date format");
        }
      
        // Create a new Date object for January 1st of the same year
        const startOfYear = new Date(date.getFullYear(), 0, 1);
      
        // Calculate the difference in milliseconds
        const timeDiff = date - startOfYear;
      
        // Calculate the day of the year (adding 1 to start from 1 instead of 0)
        const dayOfYear = Math.floor(timeDiff / (1000 * 60 * 60 * 24)) + 1;
      
        return dayOfYear;
      }
      
}

module.exports=YearlyDataMap;