class LocationUtil{

    static convertLatitude(latitudeObj) {
      let latitude=latitudeObj.value;
      const latDirection = latitude >= 0 ? 'N' : 'S';
      latitude = Math.abs(latitude);
      const Degrees = Math.floor(latitude);
      const Minutes = Math.floor((latitude - Degrees) * 60);
      const Seconds = Math.round(((latitude - Degrees) * 60 - Minutes) * 60);
      latitudeObj.displayValue=`${Degrees}° ${Minutes}' ${Seconds} "`;
      latitudeObj.unit=latDirection;
      
    }

    static convertLongitude(longitudeObj) {
      let longitude=longitudeObj.value;
      const longDirection = longitude >= 0 ? 'E' : 'W';
      
      longitude = Math.abs(longitude);
      const Degrees = Math.floor(longitude);
      const Minutes = Math.floor((longitude - Degrees) * 60);
      const Seconds = Math.round(((longitude - Degrees) * 60 - Minutes) * 60);
      longitudeObj.displayValue=`${Degrees}° ${Minutes}' ${Seconds} "`;
      longitudeObj.unit=longDirection;
      
    }

    static convertcourseAngle(courseAngleObj) {
      let courseAngle=courseAngleObj.value;
      courseAngle = courseAngle * (180 / Math.PI);
      courseAngleObj.displayValue=Math.round(courseAngle)+"°";
      courseAngleObj.unit="°";
    }
    static convertCoordinates(latitude, longitude, courseAngle) {
        const latDirection = latitude >= 0 ? 'N' : 'S';
        const longDirection = longitude >= 0 ? 'E' : 'W';
      
        latitude = Math.abs(latitude);
        longitude = Math.abs(longitude);
      
        const latDegrees = Math.floor(latitude);
        const latMinutes = Math.floor((latitude - latDegrees) * 60);
        const latSeconds = Math.round(((latitude - latDegrees) * 60 - latMinutes) * 60);
      
        const longDegrees = Math.floor(longitude);
        const longMinutes = Math.floor((longitude - longDegrees) * 60);
        const longSeconds = Math.round(((longitude - longDegrees) * 60 - longMinutes) * 60);
      
        const courseDegrees = Math.floor(courseAngle);
        const courseMinutes = Math.floor((courseAngle - courseDegrees) * 60);
        const courseSeconds = Math.round(((courseAngle - courseDegrees) * 60 - courseMinutes) * 60);
      
        const latString = `${latDegrees}° ${latMinutes}' ${latSeconds}"`;
        const longString = `${longDegrees}° ${longMinutes}' ${longSeconds}"`;
        const courseString = `${courseDegrees}° ${courseMinutes}' ${courseSeconds}"`;
        //return latString +", "+ longString+", "+courseDegrees+"°"; 
        return { latitude: latString, longitude: longString, courseAngle: courseString, latDirection:latDirection, lngDirection:longDirection };
      }
}
module.exports=LocationUtil;