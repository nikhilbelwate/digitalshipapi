class EngineUtil{
    static maxValueMap=new Map();
    static init(){
        this.maxValueMap.set("engineRPM",1800);
        this.maxValueMap.set("engineLoad",100);
        this.maxValueMap.set("fuelBurn",500);
        this.maxValueMap.set("generator",500); 
        this.maxValueMap.set("oilTemperature",200);
        this.maxValueMap.set("oilPressure",1500);
        
    }
    static mergeArray(operations){
        
        for(var [key,maxvalue] of this.maxValueMap.entries()){
                for(var i=0;i<operations[key]?.values.length;i++){
                    let param=operations[key]?.values[i];
                    param["maxValue"]=maxvalue;
                    param["value"]=Math.round(parseFloat(param?.value));
                    if(maxvalue==100)
                        param["percentValue"]=param?.value;
                    else
                        param["percentValue"]=parseFloat(parseFloat((param?.value/maxvalue)*100).toFixed(2));
                    operations[key].values[i]=param;
                }
            
        }
    }
    static formatComputedValues(computedParameters){
        for(var [key,maxvalue] of this.maxValueMap.entries()){
            if(computedParameters[key]?.value!=null){
                
                computedParameters[key]["maxValue"]=maxvalue;
                
                computedParameters[key]["value"]=Math.round(parseFloat(computedParameters[key]["value"]));
            }           
        }
    }
    

}
module.exports=EngineUtil;