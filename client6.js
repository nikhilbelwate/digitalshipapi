class EngineUtil{
    static maxValueMap=new Map();
    static init(){
        this.maxValueMap.set("engineRPM",1800);
        this.maxValueMap.set("engineLoad",100);
        this.maxValueMap.set("fuelBurn",500);
        this.maxValueMap.set("generator",500); 
        this.maxValueMap.set("oilTemperature",200);
        this.maxValueMap.set("oilPressure",1500);
        
    }
    static mergeArray(operations){
        
        for(var [key,maxvalue] of this.maxValueMap.entries()){
            if(Array.isArray(operations[key]?.values)){
                operations[key].values=operations[key]?.values.map(param=>{ 
                    param["maxValue"]=maxvalue;
                    param["percentValue"]=parseFloat(parseFloat((param["value"]/maxvalue)*100).toFixed(2));
                    return param;
                })
            }
        }
        
        return operations;
    }
    

}

var data={
    "operatingParameters":{
        "engineRPM":{
            "displayName": "Engine Speed",
            "unit": "rpm",
            "values":
                [
                    {
                        "name": "Port",
                        "value": 1220,
                        "color": "#001A71"
                    },
                    {
                        "name": "Starboard",
                        "value": 1122,
                        "color": "#001A71"
                    }
                ]
            },
        
        "engineLoad":{
            "displayName": "Engine Load",
            "unit": "%",
            "values":
                [
                    {
                        "name": "Port",
                        "value": 90,
                        "color": "#C8102E"
                    },
                    {
                        "name": "Starboard",
                        "value": 70,
                        "color": "#001A71"
                    }
                ]
            
        },
        "fuelBurn":{
            "displayName": "Fuel Burn",
            "unit": "L/h",
            "values":
                [
                    {
                        "name": "Port",
                        "value": 500,
                        "color": "#C8102E"
                    },
                    {
                        "name": "Starboard",
                        "value": 440,
                        "color": "#001A71"
                    }
                ]
            
        },
        "generator":{
            "displayName": "Generator",
            "unit": "kW",
            "values":
                [
                    {
                        "name": "Port",
                        "value": 0,
                        "maxValue": 350,
                        "color": "#969696"
                    },
                    {
                        "name": "Starboard",
                        "value": 300,
                        "maxValue": 350,
                        "color": "#001A71"
                    }
                ]
            
        },
        "oilTemperature":{
            "displayName": "Oil Temperature",
            "unit": "C",
            "values":
                [
                    {
                        "name": "Port",
                        "value": 120,
                        "maxValue": 200,
                        "color": "#001A71"
                    },
                    {
                        "name": "Starboard",
                        "value": 112,
                        "maxValue": 200,
                        "color": "#001A71"
                    }
                ]
            
        },
        "oilPressure":{
            "displayName": "Oil Pressure",
            "unit": "kPa",
            "values":
                [
                    {
                        "name": "Port",
                        "value": 1400,
                        "color": "#001A71"
                    },
                    {
                        "name": "Starboard",
                        "value": 1390,
                        "color": "#C8102E"
                    }
                ]
    }
}
}

EngineUtil.init();
console.log(JSON.stringify(EngineUtil.mergeArray(data.operatingParameters)));