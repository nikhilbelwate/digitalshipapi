const WebSocket = require('ws');

// WebSocket server URL
//const serverUrl = 'ws://localhost:8080/'; // Replace with the appropriate server URL
const serverUrl = 'ws://localhost:8080/api/ships/ws';
// Create a WebSocket instance
const ws = new WebSocket(serverUrl);

// Event: WebSocket connection established
ws.onopen = () => {
  console.log('Connected to the WebSocket server');
  
  // Send a message to the server
  ws.send('Hello, server!');
};

// Event: Message received from the server
ws.onmessage = (event) => {
  const message = event.data;
  console.log('Received message from server:', message);
};

// Event: WebSocket connection closed
ws.onclose = () => {
  console.log('Disconnected from the WebSocket server');
};

// Event: WebSocket connection error
ws.onerror = (error) => {
  console.error('WebSocket error:', error);
};
