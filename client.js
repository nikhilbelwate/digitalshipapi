var WebSocket = require('ws');
try{
const ws = new WebSocket('ws://localhost:8080/');

ws.on('error', console.error);

ws.on('open', function open() {
  ws.send('something for server');
  console.log("sent");
});

ws.on('message', function message(data) {
  console.log('received: '+ data);
  ws.send('received -> '+ data);
});

}
catch(e){
  console.log(e);
}
