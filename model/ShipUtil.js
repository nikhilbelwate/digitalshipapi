var Validator = require("../logic/Validator");
var Formatter = require("../logic/Formatter");
var shipStructure = require("../initdata/shipStructure.json");
var StatsUtil = require('./StatsUtil');
const DBUtil = require("../db/DBUtil");
const AlertUtil = require("./AlertUtil");
class ShipUtil {
    static data = { "ships": {}, "version": "1", "timestamp": 0 }
    static ships = new Map();
    static AppStartTime = "";

    static init() {
        //To set inital static data
        var initData = require('../initdata/initdata.json');
        this.setShips(initData);
    }
    static getShipID(mmis) {
        Object.keys(this.ships).forEach((key) => {
            let s = this.ships.get(key);

            if (s.vessel.mmis === mmis) {
                return s.vessel.id;
            }
        });
    }
    static setShip(s) {
        if (s != null && s != {}) {
            if (s.hasOwnProperty("vessel")) {
                if (s.vessel.hasOwnProperty("id")) {
                    let sid = parseInt(s.vessel.id);
                    console.log("sid=" + sid);
                    if (Validator.isShipValid(s)) {
                        let fullShip = this.ships.get(sid);
                        if (fullShip) {
                            this.ships.set(sid, Formatter.apply(this.mergeObjects(fullShip, s)));
                            s=this.ships.get(sid);
                            s["alertMap"] = AlertUtil.AlertMap.get(sid);
                            //console.log(this.ships.get(sid));
                        } else {
                            if (AlertUtil.addNewShip(sid)) {
                                this.ships.set(sid, Formatter.apply(this.mergeObjects(shipStructure, s)));
                                StatsUtil.addShipStats(sid, s.vessel.name);
                                StatsUtil.shipCount = this.ships.size;
                                let s=this.ships.get(sid);
                                s["alertMap"] = AlertUtil.AlertMap.get(sid);
                            }else {
                                AlertUtil.errors.forEach(e => console.log(e));
                            }
                        }
                    }

                }
            }
        }
    }
    static setShips(d) {
        if (d != null && d != {}) {
            if (d.ships != null && d.ships != {}) {
                Object.keys(d.ships).forEach(async (key) => {
                    if (Validator.isShipValid(d.ships[key])) {
                        let s = this.ships.get(key);
                        if (s) {
                            this.ships.set(parseInt(key), Formatter.apply(this.mergeObjects(s, d.ships[key])));
                            let s=this.ships.get(parseInt(key));
                            s["alertMap"] = AlertUtil.AlertMap.get(parseInt(key));
                            //console.log(this.ships.get(key));
                        }
                        else {
                            //console.log(d.ships[key]["alert"]);
                            if (AlertUtil.addNewShip(parseInt(key))) {
                                
                                this.ships.set(parseInt(key), Formatter.apply(this.mergeObjects(shipStructure, d.ships[key])));
                                let s=this.ships.get(parseInt(key));
                                s["alertMap"] = AlertUtil.AlertMap.get(parseInt(key));
                                
                            } else {
                                AlertUtil.errors.forEach(e => console.log(e));
                            }
                        }
                        StatsUtil.shipCount = this.ships.size;
                        // StatsUtil.addShipStats(key,d.ships[key].vessel.name);
                    }
                });
            }
        }
    }

    static getShip(id) {
        //console.log(this.ships.get(parseInt(id)));
        let s=this.ships.get(parseInt(id));
        if(s?.alertMap instanceof Map){
            s["alert"]=AlertUtil.getAlertsById(parseInt(id));
        }
        return s;
    }
    static getShips() {
        this.ships.forEach((ship, sid)=>{
            if(ship?.alertMap instanceof Map)
                ship["alert"]=AlertUtil.getAlertsById(sid);
            return ship;
        })
        return Object.fromEntries(this.ships);
    }
    static getData() {
        this.data.ships = this.getShips();
        return this.data;
    }
    static mergeObjects(obj1, obj2) {
        const merged = Object.assign({}, obj1);
        //console.log("obj1=" + JSON.stringify(obj1));
        if (obj2?.currentPosition?.gps?.latitude?.value === 49.329) {
            console.log("obj2=" + JSON.stringify(obj2));
        }
        for (const key in obj2) {
            if (obj2.hasOwnProperty(key)) {
                if (typeof obj2[key] === "object" && obj2[key] !== null && !Array.isArray(obj2[key])) {
                    if (typeof obj1[key] === "object" && obj1[key] !== null && !Array.isArray(obj1[key])) {
                        merged[key] = this.mergeObjects(obj1[key], obj2[key]);
                    } else {
                        merged[key] = Object.assign({}, obj2[key]);
                    }
                } else {

                    merged[key] = obj2[key];
                }
            }
        }

        return merged;
    }

}

module.exports = ShipUtil;