
class AlertUtil {
    static AlertMap = new Map();
    static errors = new Array();
    static addNewShip(shipId) {
        if (shipId > 0) {
            this.AlertMap.set(shipId, new Map());
            return this.addNewComponent(0, shipId);
        } else {
            this.errors.unshift("Invalid ship id = " + shipId);
            return false;
        }
    }
    static addNewComponent(component_id, shipId) {
        if (shipId > 0) {
            let componentMap = this.AlertMap.get(shipId);
            if (component_id >= 0) {
                componentMap.set(component_id, new Map());
                componentMap.get(component_id).set("default", new Array());
                return true;
            } else {
                this.errors.unshift("Invalid component_id = " + component_id);
                return false;
            }
        } else {
            this.errors.unshift("Invalid ship id = " + shipId);
            return false;
        }
    }
    static addNewAlert(alert) {
        if (alert.hasOwnProperty("vessel_id")) {
            let shipId = parseInt(alert.vessel_id);
            if (shipId > 0) {
                let component_id = alert.hasOwnProperty("component_id") ? (parseInt(alert.component_id) >= 0 ? parseInt(alert.component_id) : 0) : 0;

                let info = alert.hasOwnProperty("info") ? alert.info !== null? alert.info : "default" :"default";

                let active = (alert.hasOwnProperty("is_active")) ? alert.is_active : true;

                if (typeof active != "boolean") {
                    if (typeof active == "string")
                        active = active === "false" ? false : true;
                    else if (typeof active == "number")
                        active = active === 0 ? false : true;
                    else
                        active = true;
                }
                let componentMap = this.AlertMap.get(shipId);
                if (!componentMap.has(component_id)) {
                    componentMap.set(component_id, new Map());
                }
                let infoMap = componentMap.get(component_id);
                if (!infoMap.has(info)) {
                    infoMap.set(info, new Array());
                }
                let alertArray = infoMap.get(info);
                if(active===false){
                    infoMap.delete(info);
                    if(infoMap.size==0){
                        componentMap.delete(component_id);
                    }

                }else{
                    alertArray.unshift(alert);
                    if (alertArray.length > 1) {
                        alertArray.sort(function (a, b) {
                            let x = a.hasOwnProperty("priority") ? parseInt(a.priority) : 10;
                            let y = b.hasOwnProperty("priority") ? parseInt(b.priority) : 10;
                            return x - y;
                        });
                    }
                }
                
                return true;
            } else {
                this.errors.unshift("vessel_id Not correct alert = " + JSON.stringify(alert));
                return false;
            }
        } else {
            this.errors.unshift("vessel_id not present in alert = " + JSON.stringify(alert));
            return false;
        }
    }

    static getShipAlerts(shipId) {
        console.log(this.AlertMap.get(shipId));
        return this.AlertMap.get(shipId);
    }

    static getAllAlerts() {
        let alerts = [];
        for (let [key, value] of this.AlertMap) {
            for (let infos of value.values()) {
                for (let arr of infos.values()) {
                    for (let a of arr) {
                        alerts.push(a);
                    }
                }
            }
        }
        alerts.sort(function (a, b) {
            let x = a.hasOwnProperty("priority") ? parseInt(a.priority) : 10;
            let y = b.hasOwnProperty("priority") ? parseInt(b.priority) : 10;
            return x - y;
        });
        return alerts;
    }
    static getAlertsById(vid) {
        let alerts = [];
        let value = this.AlertMap.get(vid);
        if (value instanceof Map) {
            for (let infos of value.values()) {
                if (infos instanceof Map) {
                    for (let arr of infos.values()) {
                        for (let a of arr) {
                            alerts.push(a);
                        }
                    }
                }
            }
        }
        alerts.sort(function (a, b) {
            let x = a.hasOwnProperty("priority") ? parseInt(a.priority) : 10;
            let y = b.hasOwnProperty("priority") ? parseInt(b.priority) : 10;
            return x - y;
        });
        return alerts;
    }
    static vessel_alert(map) {
        let alerts = [];
        for (let infos of map.values()) {
            if (infos instanceof Map) {
                for (let arr of infos.values()) {
                    if (arr.length > 0) {
                        for (let a of arr) {
                            alerts.push(a);
                        }
                    }
                }
            }
        }

        alerts.sort(function (a, b) {
            let x = a.hasOwnProperty("priority") ? parseInt(a.priority) : 10;
            let y = b.hasOwnProperty("priority") ? parseInt(b.priority) : 10;
            return x - y;
        });
        return alerts;
    }
    static replacer(key, value) {
        if (value instanceof Map) {
            return {
                dataType: 'Map',
                value: Array.from(value.entries()), // or with spread: value: [...value]
            };
        } else {
            return value;
        }
    }
    static reviver(key, value) {
        if (typeof value === 'object' && value !== null) {
            if (value.dataType === 'Map') {
                return new Map(value.value);
            }
        }
        return value;
    }
}

module.exports = AlertUtil;