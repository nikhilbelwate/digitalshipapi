
var initStats = require("../initdata/initstats.json");
class Stat{
        constructor(id,name){
            this.vessel=name;
            this.id=id;
            this.vesselMaintenance={
                "PMJobs": 0,
                "NonPMJob": 0,
                "WorkOrder": 0,
                "certNext3M": 0,
                "certNext6M": 0,
                "certNext9M": 0,
                "standardJob": 1,
                "workOrderByCrew": 0,
                "standardOperation": 0,
                "standardRequistion": 0
            };
            this.vesselOperationStatus={
                "underway": 10,
                "maintenance": 0,
                "idleCrewOn": 10,
                "idleCrewOff": 700
            };
            console.log("stats in construct "+ id+"  "+name);
        }
}
class StatsUtil{
    static stats = new Object();
    static shipCount=0; 
    static init(){
        var initStats = require('../initdata/initstats.json');
       
        this.setStats(initStats);
    }
    static addShipStats(id,name){
        let shipStat=new Stat(id,name);
        if(this.stats["ships"]!==undefined)
            this.stats["ships"][id]=shipStat;
        else{
            this.stats=initStats;
            
        }
        console.log("stats ship added "+name+ " Total="+ JSON.stringify(this.stats));
    }
    static getStats() {
        if (this.stats.hasOwnProperty("ships")) {
            return this.stats;
        } else {
            return initStats;
        }
    }
    static setStats(pushedStats) {
        let ships = {};
        if (this.stats.hasOwnProperty("ships")) {
            ships = this.stats.ships;
        } else {
            ships = initStats.ships;
        }
        if (pushedStats.hasOwnProperty("ships")) {
            Object.entries(pushedStats.ships).map(([k, s]) => {
                ships[k] = s;
            });
        }
        this.stats["ships"] = ships;
    }
    static sumOfObjects(ship1, ship2) {
        const sumObj = JSON.parse(JSON.stringify(ship1));
        let obj1 = ship1.vesselMaintenance;
        let obj2 = ship2.vesselMaintenance;
        for (const key in obj2) {
            if (obj2.hasOwnProperty(key)) {

                sumObj.vesselMaintenance[key] = parseFloat(obj1[key]) + parseFloat(obj2[key]);
            }
        }
        obj1 = ship1.vesselOperationStatus;
        obj2 = ship2.vesselOperationStatus;
        for (const key in obj2) {
            if (obj2.hasOwnProperty(key)) {

                sumObj.vesselOperationStatus[key] = parseFloat(obj1[key]) + parseFloat(obj2[key]);
            }
        }
        return sumObj;
    }
    static getMonthlyPercentage(sum) {
        if(this.shipCount>0){
            let avg=(sum/this.shipCount);
            return parseFloat(((avg / 720) * 100).toFixed(2));
        }else{
            return 0.0;
        }
    }
}

module.exports=StatsUtil;