class SIMShip {
    static data = { "ships": {}, "version": "1", "timestamp": 0 }
    static ships = new Map();
    static init() {
        //To set inital static data
        var initData = require('../initdata/SIMinitdata.json');
        this.ships.set(1,initData[0]);
    }
    static setShip(s) {
        if (s != null && s != {}) {
            if (s.hasOwnProperty("vessel")) {
                if (s.vessel.hasOwnProperty("id")) {
                    let sid = parseInt(s.vessel.id);
                    console.log("sid=" + sid);
                    let fullShip = this.ships.get(sid);
                    if(fullShip){
                        this.ships.set(sid, this.mergeObjects(fullShip, s));
                    }else{
                        this.ships.set(sid, s);
                    }
                }
            }
        }
    }
    static getShip(id) {
        //console.log(this.ships.get(parseInt(id)));
        let s=this.ships.get(parseInt(id));
        if(s)
            return s;
        else
            return {};
    }
    static getData() {
        this.data.ships = Object.fromEntries(this.ships);
        return this.data;
    }
    static mergeObjects(obj1, obj2) {
        const merged = Object.assign({}, obj1);
        //console.log("obj1=" + JSON.stringify(obj1));
        if (obj2?.currentPosition?.gps?.latitude?.value === 49.329) {
            console.log("obj2=" + JSON.stringify(obj2));
        }
        for (const key in obj2) {
            if (obj2.hasOwnProperty(key)) {
                if (typeof obj2[key] === "object" && obj2[key] !== null && !Array.isArray(obj2[key])) {
                    if (typeof obj1[key] === "object" && obj1[key] !== null && !Array.isArray(obj1[key])) {
                        merged[key] = this.mergeObjects(obj1[key], obj2[key]);
                    } else {
                        merged[key] = Object.assign({}, obj2[key]);
                    }
                } else {

                    merged[key] = obj2[key];
                }
            }
        }

        return merged;
    }
}
module.exports = SIMShip;
    