const AlertUtil = require("./AlertUtil");

class WebSocketUtil {
  static wsInstance;
  static CLIENTS = new Map();
  static ALERTCLIENTS = new Map();
  static broadcast(msg) {
    this.wsInstance.getWss().clients.forEach((client) => {
      //console.log(JSON.stringify(client));
      if (client.readyState === 1) {
        client.send(msg);
      }
    });
  }
  static saveClient(cid, client) {
    let clientArray = this.CLIENTS.get(cid);
    console.log("connected for "+cid);
    if (clientArray) {
      clientArray.push(client);
    } else {
      clientArray = new Array();
      clientArray.push(client);     
      this.CLIENTS.set(parseInt(cid),clientArray);
    }
    console.log("clientArray="+JSON.stringify(clientArray));
    console.log("saved clients=" + JSON.stringify(this.CLIENTS));
    
  }
  static saveAlertClient(cid, client) {
    let clientArray = this.ALERTCLIENTS.get(cid);
    console.log("connected for alerts of "+cid);
    if (clientArray) {
      clientArray.push(client);
    } else {
      clientArray = new Array();
      clientArray.push(client);     
      this.ALERTCLIENTS.set(parseInt(cid),clientArray);
    }
    console.log("alerts clientArray="+JSON.stringify(clientArray));
    console.log("saved alerts clients=" + JSON.stringify(this.ALERTCLIENTS));
    
  }

  static remove(cid) {
    let clientArray = this.CLIENTS.get(cid);
    if (clientArray) {
      for (let i = 0; i < clientArray.length; i++) {
        if (clientArray[i].readyState !== 1) {
          clientArray.splice(i, 1);
        }
      }
    }
  }

  static broadcast(msg, cid) {
    console.log("broadcasted msg to=" + cid);
    let clientArray = this.CLIENTS.get(parseInt(cid));
    //console.log("clients=" + JSON.stringify(this.CLIENTS));
    if (clientArray) {
      for (let i = 0; i < clientArray.length; i++) {
        //console.log(JSON.stringify(clientArray[i]));
        if (clientArray[i].readyState === 1) {
          try{
          clientArray[i].send(JSON.stringify(msg));
          
          }catch(e){
            console.log(e);
          }
        } else {
          clientArray.splice(i, 1);
        }
      }
    }
  }

  static broadcastAlerts(alerts, cid) {
    console.log("broadcasted alerts to=" + parseInt(cid));
    let clientArray = this.ALERTCLIENTS.get(parseInt(cid));
    //console.log("clients=" + JSON.stringify(this.CLIENTS));
    if (clientArray) {
      for (let i = 0; i < clientArray.length; i++) {
        //console.log(JSON.stringify(clientArray[i]));
        if (clientArray[i].readyState === 1) {
          try{
          clientArray[i].send(alerts);
          }catch(e){
            console.log(e);
          }
        } else {
          clientArray.splice(i, 1);
        }
      }
    }
  }
}

module.exports = WebSocketUtil;