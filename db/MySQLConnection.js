const mysql = require('mysql2');
const dbConfig = require('../config.json');
delete dbConfig.WS_TOKEN;
class MySQLConnection {
  constructor() {
    this.connection = mysql.createConnection(dbConfig);
    console.log("connect to MySQL");
  }

  query(sql, values = []) {
    return new Promise((resolve, reject) => {
      this.connection.query(sql, values, (error, results) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
      });
    });
  }

  close() {
    return new Promise((resolve, reject) => {
      this.connection.end((error) => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    });
  }
}

module.exports = MySQLConnection;
