const YearlyDataMap = require('../reducers/YearlyDataMap');
const MySQLConnection = require('./MySQLConnection'); // Replace with your file path

class DBUtil {
    static porpertyMap = new Map();
    static init() {
        this.porpertyMap.set("Port Main Engine Fuel", "Port_MainEngine_FuelRate");
        this.porpertyMap.set("Port Main Engine Speed", "Port_MainEngine_EngineSpeed");
        this.porpertyMap.set("Port Main Engine Load", "Port_MainEngine_PercentLoadAtCurrentSpeed");
        this.porpertyMap.set("Starboard Main Engine Fuel", "Starboard_MainEngine_FuelRate");
        this.porpertyMap.set("Starboard Main Engine Speed", "Starboard_MainEngine_EngineSpeed");
        this.porpertyMap.set("Starboard Main Engine Load", "Starboard_MainEngine_PercentLoadAtCurrentSpeed");
    }
    static async getTempVesselComponents(vId) {
        let results = [];
        try {
            let db = new MySQLConnection();
            let rootId = 0;
            results = (await db.query('SELECT * FROM  vessel_alert_component WHERE vessel_id='+vId)).map((item) => {
                item["parentId"] = item["vessel_parent_component_id"];
                item["id"]=item["vessel_component_id"];
                item["is_active"]=1;
                item["component_id"]=item["id"];
                item["imageUrl"] = (item["icon"] !== undefined && item["icon"] !== null) ? "/images/"+item["icon"]+".svg" : "/images/engine.svg";
                delete item.vessel_parent_component_id;
                delete item.vessel_component_id;
                delete item.vessel_id;
                return Object.assign({}, item);
            });
                db.close();
                return results;
            } catch (error) {
                console.error('Error:', error);
                return [];
            }
    }
    static async getVesselComponents(vId) {
        let results = [];
        try {
            let db = new MySQLConnection();
            let rootId = 0;
            results = (await db.query('SELECT * FROM  component_mapping RIGHT JOIN vessel_component ON component_mapping.component_id=vessel_component.id')).map((item) => {
                item["parentId"] = item["parent_id"];
                if (item.parentId === null) {
                    item["parentId"] = rootId;
                }
                item["imageUrl"] = (item["imageUrl"] !== undefined && item["imageUrl"] !== null) ? item["imageUrl"] : "/images/engine.svg";

                // item["design"]=(item["design"]!==undefined && item["design"]!==null)?item["design"]:0;
                item["maintenance_history"] = (item["maintenance_history"] !== undefined && item["maintenance_history"] !== null) ? item["maintenance_history"] : 0;
                //Temp adjustment with DB later mate all 1 as 0
                item["design"] = item["design"] === null ? 1 : item["design"];
                item["history"] = item["history"] === null ? 1 : item["history"];
                item["alarms"] = item["alarms"] === null ? 1 : item["alarms"];
                item["anomaly"] = item["anomaly"] === null ? 1 : item["anomaly"];
                item["vibration"] = item["vibration"] === null ? 1 : item["vibration"];
                item["monitoring"] = item["monitoring"] === null ? 1 : item["monitoring"];
                item["maintenance_due"] = item["maintenance_due"] === null ? 1 : item["maintenance_due"];
                delete item.parent_id;
                return Object.assign({}, item);
            });
            let vessel = await db.query('SELECT * FROM vessel WHERE id=' + vId);
            console.log(vessel);
            results.unshift({ id: rootId, imageUrl: '/images/boat.png', name: vessel[0].name, detalis: "mmis:" + vessel[0].mmis, parentId: null });
            //console.log('Query Results:', results);
            // Perform other database operations
            // return results;
            db.close();
            return results;
        } catch (error) {
            console.error('Error:', error);
            return [];
        }
    }
    static async getComponentDocFileId(vId,component_id){

        try {
            let db = new MySQLConnection();
            let sql = "select * from aras_component_file WHERE vessel_id=" + vId + " AND vessel_component_id=" + component_id;
            let fileArray=new Array();
            (await db.query(sql)).map((item) => {
                delete item.id;
                delete item.vessel_id;
                delete item.vessel_component_id;
                item.token="";
                fileArray.push(item);
                return item;
            });
            db.close();
            return fileArray;
        } catch (error) {
            console.error('Error:', error);
            return [];
        }
    }
    static async getAlertHistory(vessel_id, component_id) {
        try {
            let db = new MySQLConnection();
            let sql = "SELECT * FROM alert_history WHERE vessel_id=" + vessel_id + " AND component_id=" + component_id + " ORDER BY date_created DESC";
            let alertArray = new Array();
            (await db.query(sql)).map((item) => {
                alertArray.push(item);
                return item;
            });
            db.close();
            return alertArray;
        } catch (error) {
            console.error('Error:', error);
            return [];
        }
    }
    static async getFleetStatus() {
        try {
            let db = new MySQLConnection();
            let sql = "SELECT name, fleet_status," +
                " sum(duration) as totalTime FROM dsiss.view_towworks_events" +
                " WHERE (start_time BETWEEN date_sub(now(), INTERVAL 30 DAY) AND now()) AND (vessel_id =4 OR vessel_id = 5) GROUP BY name, fleet_status;";
            let stateMap = new Map();
            //let totalTimeInMin=0;
            (await db.query(sql)).map((item) => {
                //logic pending
                if (stateMap.has(item["fleet_status"])) {
                    stateMap.set(item["fleet_status"], stateMap.get(item["fleet_status"]) + parseInt(item["totalTime"]));
                } else {
                    stateMap.set(item["fleet_status"], parseInt(item["totalTime"]));
                }
                //   totalTimeInMin=totalTimeInMin+parseInt(item["totalTime"]);
                return item;
            });
            // let timeIdleCrewOff=86400-totalTimeInMin;
            let output = [
                { name: "Underway", value: stateMap.get("Underway") > 0 ? parseFloat(stateMap.get("Underway") / 864).toFixed(2) : 0 },
                { name: "Idle-Crew-On", value: stateMap.get("Idle - Crew On") > 0 ? parseFloat(stateMap.get("Idle - Crew On") / 864).toFixed(2) : 0 },
                { name: "Maintenance", value: stateMap.get("Maintenance") > 0 ? parseFloat(stateMap.get("Maintenance") / 864).toFixed(2) : 0 },
                { name: "Idle-Crew-Off", value: stateMap.get("Idle - Crew Off") > 0 ? parseFloat(stateMap.get("Idle - Crew Off") / 864).toFixed(2) : 0 },
            ];
            db.close();
            return output;
        } catch (error) {
            console.error('Error:', error);
            return [];
        }
    }
    static async getMaintenanceDueIn60Days() {
        try {
            let db = new MySQLConnection();
            let count = 0;
            (await db.query('SELECT COUNT(*) as records FROM dsiss.absns_maintenance_plan'
                + ' WHERE (vessel_id = 4 or vessel_id = 5) and class_society_req = 1'
                + ' and next_occur_date between now() and date_add(now(),interval 60 day)')).map((item) => {
                    count = item["records"];
                    console.log(count);
                });
            db.close();
            return count;
        } catch (error) {
            console.error('Error:', error);
            return 0;
        }
    }
    static async getScheduledNext30days() {
        try {
            let db = new MySQLConnection();
            let count = 0;
            (await db.query('SELECT COUNT(*) as records FROM dsiss.absns_maintenance_schedule'
                + ' WHERE (vessel_id = 4 or vessel_id = 5)'
                + ' and scheduled_date between now() and date_add(now(), interval 30 day)')).map((item) => {
                    count = item["records"];
                    console.log(count);
                });
            db.close();
            return count;
        } catch (error) {
            console.error('Error:', error);
            return 0;
        }
    }
    static async getOverdueScheduledCount() {
        try {
            let db = new MySQLConnection();
            let count = 0;
            (await db.query('SELECT COUNT(*) as records FROM dsiss.absns_maintenance_schedule'
                + ' WHERE (vessel_id = 4 or vessel_id = 5)'
                + ' and scheduled_date < date_sub(now(), interval 1 day) AND completed_date is null')).map((item) => {
                    count = item["records"];
                    console.log(count);
                });
            db.close();
            return count;
        } catch (error) {
            console.error('Error:', error);
            return 0;
        }
    }


    static async saveAlert(alert) {

        let column = "(vessel_id, component_id, type, priority, value, message, info, is_active, extra)";
        let values = alert?.vessel_id + ", " + alert?.component_id + ", '" + alert?.type + "', " + alert?.priority + ", " + alert?.value + ", '" + alert?.message + "','" + alert?.info + "'," + alert?.is_active + ",'" + alert?.extra + "'";
        let sql = "INSERT INTO alert_history" + column + " VALUES(" + values + ")";
        console.log(sql);
        try {
            let db = new MySQLConnection();
            let res = await db.query(sql);
            res = res.affectedRows > 0;
            db.close();
            return res;
        } catch (error) {
            console.error('Error:', error);
            return false;
        }
    }
    static convertDateFormat(inputDate) {
        // Split the input date into parts
        const parts = inputDate.split('/');

        // Create a new Date object using the parts
        const dateObject = new Date(parts[2], parts[0] - 1, parts[1]);

        // Extract the year, month, and day from the date object
        const year = dateObject.getFullYear();
        const month = String(dateObject.getMonth() + 1).padStart(2, '0');
        const day = String(dateObject.getDate()).padStart(2, '0');

        // Construct the new formatted date
        const newDate = `${year}-${month}-${day}`;

        return newDate;
    }
    static dayDifferenc(start, end) {
        // Split the input date into parts
        let parts = start.split('/');
        const startDate = new Date(parts[2], parts[0] - 1, parts[1]);
        parts = end.split('/');
        const endDate = new Date(parts[2], parts[0] - 1, parts[1]);
        // Extract the year, month, and day from the date object
        let days = 1;
        if (startDate.getDate() > endDate.getDate()) {
            days = (endDate.getDate() + 30) - startDate.getDate();
        } else if (startDate.getDate() < endDate.getDate()) {
            days = endDate.getDate() - startDate.getDate();
        } else {
            days = 1;
        }
        days < 0 ? days = days * -1 : days == 0 ? days = 1 : days = days * 1;
        return days;
    }
    static async getTimeAndData(vId, start, end, offset,selectedProps) {

        let combineResult = [];
        if (start !== undefined && end !== undefined) {
            let days = this.dayDifferenc(start, end);
            start = this.convertDateFormat(start);
            end = this.convertDateFormat(end);

            let columnName = "floor(unix_timestamp(Vessel_Timestamp)/60/" + days + ") * 60 * " + days + " as minute_timestamp";
            let valueCondition = "vessel_id =" + vId + " and Vessel_Timestamp>'" + start + " 00:00:00' and Vessel_Timestamp<'" + end + " 23:59:00' group by minute_timestamp";
            for (let prop of selectedProps) {
                let timeData = new Object();
                timeData["label"] = prop;
                timeData["value"] = prop.replace("Main Engine ", "");
                let data = [];
                columnName = columnName + ", " + "GREATEST(0, avg(" + this.porpertyMap.get(prop) + ")) as " + this.porpertyMap.get(prop);
                //valueCondition = valueCondition + "avg(" + this.porpertyMap.get(prop) + ") > 0 AND ";
                timeData["data"] = data;
                combineResult.push(timeData);
            }
            //valueCondition = valueCondition.substring(0, valueCondition.length - 5);
            let sql = "SELECT " + columnName + " FROM vessel_tugboat_data"
                + " WHERE " + valueCondition
                + " ORDER BY minute_timestamp asc";
            //console.log(sql);
            try {
                let db = new MySQLConnection();
                let result = await db.query(sql);
                //console.log(sql);
                result.forEach(row => {
                    for (let i = 0; i < selectedProps.length; i++) {
                        // Convert minutes to milliseconds
                      
                        let utcDate = new Date(row["minute_timestamp"] * 1000);
                        // Get the local time zone offset in minutes
                        
                        // Calculate the local time by adding the offset to the UTC time
                        const localTime = new Date(utcDate.getTime() - (offset * 60000));

                        combineResult[i]["data"].push({ date: localTime.toISOString(), value: row[this.porpertyMap.get(selectedProps[i])] });
                    }
                });

                db.close();
            } catch (error) {
                console.error('Error:', error);
            }
        }
        //console.log("data lenth",combineResult[0].data.length);
        return combineResult;
    }

    static async getMonitoringData(vId, start, end, selectedProps = "") {

        let combineResult = [];
        if (start !== undefined && end !== undefined) {
            start = this.convertDateFormat(start);
            end = this.convertDateFormat(end);
            let columnName = "pipeline.date, sensor.name, drift.drift";
            let valueCondition = "pipeline.vessel_id =" + vId + " and pipeline.date BETWEEN '" + start + " 00:00:00' and '" + end + " 00:00:00'";
            /*for (let prop of selectedProps) {
                let timeData = new Object();
                timeData["label"] = prop;
                timeData["value"] = prop.replace("Main Engine ", "");
                let data = [];
                columnName = columnName + ", " + this.porpertyMap.get(prop);
                valueCondition = valueCondition + this.porpertyMap.get(prop) + " > 0 OR ";
                timeData["data"] = data;
                combineResult.push(timeData);
            }*/
            //valueCondition = valueCondition + "id<0)";
            let sql = "SELECT " + columnName + " FROM ds_sensor_drift drift "
                + "INNER JOIN ds_drift_pipeline pipeline ON pipeline.id = drift.ds_drift_pipeline_id "
                + "INNER JOIN vessel_sensor sensor ON drift.vessel_sensor_id = sensor.id"
                + " WHERE " + valueCondition
                + " ORDER BY pipeline.date";
            console.log(sql);
            try {
                let db = new MySQLConnection();
                let result = await db.query(sql);
                //console.log(sql);
                result.forEach(row => {
                    /*for (let i = 0; i < selectedProps.length; i++) {
                        combineResult[i]["data"].push({ date: Date.parse(row["Vessel_Timestamp_UTC"]), value: row[this.porpertyMap.get(selectedProps[i])] });
                    }*/
                    //data[1]["data"].push({ date: Date.parse(row["timestm"]), value: row["value_port"] });
                    let keys = row["name"].split('_');
                    if (keys?.length == 4) {
                        row["engine"] = keys[0];
                        row["graph"] = keys[1];
                        row["displayName"] = keys[1].replace("Engine", " Engine");
                        row["param"] = keys[2];
                    }
                    combineResult.push(Object.assign({}, row));
                });

                db.close();
            } catch (error) {
                console.error('Error:', error);
            }
        }
        return combineResult;
    }
    static async getEngineLoadRecords(vId, start = undefined, end = undefined) {
        let data = [

            {
                value: "starboard",
                label: "StarBoard Main Engine Load",
                data: []
            },

            {
                value: "port",
                label: "Port Main Engine Load",
                data: []
            },

        ];
        try {
            if (start !== end) {
                let db = new MySQLConnection();
                start = this.convertDateFormat(start);
                end = this.convertDateFormat(end);
                let sql = "SELECT Vessel_Timestamp_UTC as timestm, Port_MainEngine_PercentLoadAtCurrentSpeed as value_port, Starboard_MainEngine_PercentLoadAtCurrentSpeed as value_starboard FROM vessel_tugboat_data"
                    + " WHERE vessel_id =" + vId + " and (Port_MainEngine_PercentLoadAtCurrentSpeed > 0 OR Starboard_MainEngine_PercentLoadAtCurrentSpeed > 0)"
                    + " and Vessel_Timestamp>'" + start + "' and Vessel_Timestamp<'" + end + "' ORDER BY Vessel_Timestamp asc limit 10";

                let result = await db.query(sql);
                console.log(sql);
                result.forEach(row => {
                    data[0]["data"].push({ date: Date.parse(row["timestm"]), value: row["value_starboard"] });
                    data[1]["data"].push({ date: Date.parse(row["timestm"]), value: row["value_port"] });
                });

                db.close();
            }
            return data;
        } catch (error) {
            console.error('Error:', error);
            return data;
        }
    }
    static async getPerformanceData(vId, start = undefined, end = undefined) {
        
        let combineResult = [];
        try {
            if (start !== undefined && end !== undefined) {
                start = this.convertDateFormat(start);
                end = this.convertDateFormat(end);
                let db = new MySQLConnection();
                let sql="SELECT * FROM dsiss.view_performance_analysis where vessel_id="+vId+" and date between '"
        +start+"' and '"+end+"' order by date asc";
                let result = await db.query(sql);
                console.log(sql);
                result.forEach(row => {
                    let dt= row["date"].toISOString().split('T')[0];
                    let performance = { date:dt , value: row["value"], name: row["parameter"], unit: row["unit"] }
                    combineResult.push(performance);
                });
                db.close();
            }
            return combineResult;
        } catch (error) {
            console.error('Error:', error);
            return combineResult;
        }
    }
    static async getCurrentJobsData(vId) {
        let combineResult = [];
        let sql="SELECT t.TASKGUID, w.WorkOrderStatusName , t.TASKTYPEMODULE, t.TASK_NAME,"+
        "w.EstimatedStartDate, w.EstimatedCompletionDate, w.ActualStartDate, w.ActualCompletionDate, "+
        "t.From_Location, t.To_location, t.fleet_status, w.Barge_ship, w.BillingCompanyName, w.BillToCustomerName, w.DISPATCHNOTES, w.WorkOrderInstructions,"+
        "GROUP_CONCAT(buddy.vessel_id ORDER BY buddy.vessel_id SEPARATOR ', ') AS with_vessel "+
        "FROM dsiss.towworks_task t left join dsiss.towworks_work_order w on t.LinkedWorkOrderGuid = w.WORKORDERGUID "+
        "left join dsiss.towworks_task buddy "+
        "on t.LinkedWorkOrderGuid = buddy.LinkedWorkOrderGuid and t.vessel_id != buddy.vessel_id "+
        "where t.vessel_id = "+vId+" "+
        "and t.EstimatedCompletionDate >= now() - interval 1 day "+
        "and w.WorkOrderStatusName in ('Dispatched', 'In Progress', 'In Planning','Created') "+
        "group by t.TASKGUID, w.WorkOrderStatusName, t.TASKTYPEMODULE, t.TASK_NAME, w.EstimatedStartDate, w.EstimatedCompletionDate, w.ActualStartDate, w.ActualCompletionDate,"+ 
        "t.From_Location, t.To_location, t.fleet_status, "+
        "w.Barge_ship, w.BillingCompanyName, w.BillToCustomerName, w.DISPATCHNOTES, w.WorkOrderInstructions order by w.EstimatedStartDate";
        try{
            let db = new MySQLConnection();
            let result = await db.query(sql);
            console.log(sql);
            result.forEach(row => {
                let job = {from:row["From_Location"],to:row["To_location"],with_vessel:row["with_vessel"], type:row["TASKTYPEMODULE"], customer:row["BillToCustomerName"],company:row["BillingCompanyName"],
                    startDate: new Date(row["ActualStartDate"]).getTime(), endDate: new Date(row["ActualCompletionDate"]).getTime(), name: row.TASK_NAME,
                    Barge_ship:row["Barge_ship"],fleet_status:row["fleet_status"],EstimatedStartDate:new Date(row["EstimatedStartDate"]),EstimatedCompletionDate:new Date(row["EstimatedCompletionDate"])
                }
                combineResult.push(job);
            });
            db.close();
        
        return combineResult;
    } catch (error) {
        console.error('Error:', error);
        return combineResult;
    }
    }
    //schedule related DBFunctions
    static async getJobsTimelyData(vId, start = undefined, end = undefined) {
        let combineResult = [];
        try {
            if (start === undefined && end === undefined) {
                end=new Date();
                // Extract the year, month, and day from the date object
                let year = end.getFullYear();
                let month = String(end.getMonth() + 1).padStart(2, '0');
                let day = String(end.getDate()).padStart(2, '0');

               // Construct the new formatted date
                end = `${year}-${month}-${day} 23:59:59`;

                start=new Date(new Date().setDate(new Date().getDate() - 7));
                // Extract the year, month, and day from the date object
                year = start.getFullYear();
                month = String(start.getMonth() + 1).padStart(2, '0');
                day = String(start.getDate()).padStart(2, '0');

                
               // Construct the new formatted date
               start = `${year}-${month}-${day} 00:00:00`;
            }
            else{
                start = this.convertDateFormat(start)+' 00:00:00';
                end = this.convertDateFormat(end)+' 23:59:59';
            }
                let db = new MySQLConnection();
                let sql ="SELECT t.TASKGUID, t.TASKSTATUSNAME, t.TASKTYPEMODULE, t.TASK_NAME, t.EstimatedStartDate, t.EstimatedCompletionDate, t.ActualStartDate, t.ActualCompletionDate,"+ 
                "t.From_Location, t.To_location, t.fleet_status, w.Barge_ship, w.BillingCompanyName, w.BillToCustomerName, w.DISPATCHNOTES, w.WorkOrderInstructions,"+
                "GROUP_CONCAT(buddy.vessel_id ORDER BY buddy.vessel_id SEPARATOR ', ') AS with_vessel "+
                "FROM dsiss.towworks_task t left join dsiss.towworks_work_order w on t.LinkedWorkOrderGuid = w.WORKORDERGUID left join dsiss.towworks_task buddy "+
                "on t.LinkedWorkOrderGuid = buddy.LinkedWorkOrderGuid and t.vessel_id != buddy.vessel_id "+
                "where t.vessel_id =" + vId + 
                " and t.EstimatedCompletionDate BETWEEN '" + start + "' AND '" + end + "' "+
                "group by t.TASKGUID, t.TASKSTATUSNAME, t.TASKTYPEMODULE, t.TASK_NAME, t.EstimatedStartDate, t.EstimatedCompletionDate, t.ActualStartDate, t.ActualCompletionDate,"+ 
                "t.From_Location, t.To_location, t.fleet_status, w.Barge_ship, w.BillingCompanyName, w.BillToCustomerName, w.DISPATCHNOTES, w.WorkOrderInstructions "+
                "order by t.EstimatedStartDate asc";
                /*let sql = "SELECT vessel_id, event_name, start_time, end_time FROM dsiss.towworks_events "
                    + "WHERE vessel_id = " + vId + " AND start_time BETWEEN '" + start + " 00:00:00' AND '" + end + " 00:00:00' "
                    + "order by start_time"*/
                let result = await db.query(sql);
                console.log(sql);
                result.forEach(row => {
                    let job = {from:row["From_Location"],to:row["To_location"],with_vessel:row["with_vessel"], type:row["TASKTYPEMODULE"], customer:row["BillToCustomerName"],company:row["BillingCompanyName"],
                        startDate: new Date(row["ActualStartDate"]).getTime(), endDate: new Date(row["ActualCompletionDate"]).getTime(), name: row.TASK_NAME,
                        Barge_ship:row["Barge_ship"],fleet_status:row["fleet_status"],EstimatedStartDate:new Date(row["EstimatedStartDate"]),EstimatedCompletionDate:new Date(row["EstimatedCompletionDate"])
                    }
                    combineResult.push(job);
                });
                db.close();
            
            return combineResult;
        } catch (error) {
            console.error('Error:', error);
            return combineResult;
        }
    }
    static async getStatusTimelyData(vId, start = undefined, end = undefined) {
        let combineResult = [];
        try {
            if (start !== undefined && end !== undefined) {
                start = this.convertDateFormat(start);
                end = this.convertDateFormat(end);
                let db = new MySQLConnection();
                let sql = "SELECT vessel_id, fleet_status, start_time, end_time FROM dsiss.towworks_events "
                    + "WHERE vessel_id = " + vId +" "
                    +"AND start_time BETWEEN '" + start + " 00:00:00' AND '" + end + " 00:00:00' "
                    +"order by start_time";
                let result = await db.query(sql);
                console.log(sql);
                result.forEach(row => {
                    let status = {
                        startDate: new Date(row["start_time"]).getTime(), endDate: new Date(row["end_time"]).getTime(), name: row.fleet_status
                    }
                    combineResult.push(status);
                });
                db.close();
            }
            return combineResult;
        } catch (error) {
            console.error('Error:', error);
            return combineResult;
        }
    }
    static async getScatterDataByType(vId, start=undefined,end=undefined){
        let combineResult = new Map();
        try {
            if (start !== undefined && end !== undefined) {
                start = this.convertDateFormat(start);
                end = this.convertDateFormat(end);
                let db = new MySQLConnection();
                let sql = "SELECT type,count(type) as countNo,completed_date FROM dsiss.absns_maintenance_schedule "+
                "WHERE vessel_id = " + vId +" AND completed_date BETWEEN '" + start + " 00:00:00' AND '" + end + " 00:00:00' "+
                "GROUP BY completed_date, type ORDER BY completed_date DESC"
                let result = await db.query(sql);
                console.log(sql);
                result.forEach(row => {
                    let rec={};
                    if(combineResult.has(new Date(row["completed_date"]).getTime())){
                        rec=combineResult.get(new Date(row["completed_date"]).getTime());
                    }else{
                        rec ={ date: new Date(row["completed_date"]).getTime()}
                    }
                    
                    rec[row['type']]=row['countNo'];
                    combineResult.set(new Date(row["completed_date"]).getTime(), rec);
                });
                db.close();
            }
            return combineResult;
        } catch (error) {
            console.error('Error:', error);
            return combineResult;
        }
    }
    static async getScatterChartData(vId, start = undefined, end = undefined) {
        let combineResult = [];
        try {
            if (start !== undefined && end !== undefined) {
                start = this.convertDateFormat(start);
                end = this.convertDateFormat(end);
                let db = new MySQLConnection();
                let sql = `SELECT vessel_id,
CASE
    WHEN (scheduled_date < now() AND completed_date IS NULL) THEN 'NOT-COMPLETED'
    WHEN type NOT LIKE 'PM' THEN 'UNPLANNED'
    WHEN status LIKE 'SCHEDULED' THEN 'SCHEDULED'
    ELSE 'COMPLETED'
END AS 'status',
CASE 
	WHEN type NOT LIKE 'PM' THEN 'UNPLANNED'
    ELSE 'PLANNED'
END AS type,
job_name, scheduled_date, completed_date `
                    + "FROM dsiss.absns_maintenance_schedule WHERE vessel_id = " + vId
                    + " AND scheduled_date BETWEEN '" + start + " 00:00:00' AND '" + end + " 00:00:00'"
                    + " ORDER BY scheduled_date";
                sql.replace('\n','');
                let result = await db.query(sql);
                console.log(sql);
                result.forEach(row => {
                    let rec = {
                        name: row.job_name,type:row.type, status: row.status, date: new Date(row["scheduled_date"]).getTime()
                    }
                    combineResult.push(rec);
                });
                db.close();
            }
            return combineResult;
        } catch (error) {
            console.error('Error:', error);
            return combineResult;
        }
    }

    static async getTimePlottingData(vId, start = undefined, end = undefined) {
        let combineResult = [];
        try {
            if (start !== undefined && end !== undefined) {
                start = this.convertDateFormat(start);
                end = this.convertDateFormat(end);
                let db = new MySQLConnection();
                let sql = " SELECT * FROM dsiss.absns_certificate_inference WHERE vessel_id = " + vId
                    + " AND date_scheduled BETWEEN '" + start + " 00:00:00' AND '" + end + " 23:59:59'"
                    + " ORDER BY date_scheduled";
                let result = await db.query(sql);
                console.log(sql);
                
                result.forEach(row => {
                    
                    let job =row?.job_name;
                    let rec = {item:job}
                    let isNew=true;
                    for(let d of combineResult){
                        if(d.item === rec.item){
                            rec=d;
                            isNew=false;
                            break;
                        }else{
                            isNew=true;
                        }
                    }
                    //console.log("isNew",isNew)
                    if(isNew){
                        let yearlyData=new YearlyDataMap(row);
                        Object.assign(rec,yearlyData.getYearMap());
                        rec["yearlyData"]=yearlyData;
                        combineResult.push(rec);  
                        //console.log(JSON.stringify(rec));
                    }else{
                        let yearlyData=rec?.yearlyData;
                        if(yearlyData!=null || yearlyData!={}){
                            yearlyData?.addRecord(row);
                            Object.assign(rec,yearlyData?.getYearMap());  
                        }
                        //console.log(JSON.stringify(rec));
                    }
                });
                db.close();
            }
            return combineResult;
        } catch (error) {
            console.error('Error:', error);
            return combineResult;
        }
    }

    static async getCountForPlottingData(vId, start = undefined, end = undefined) {
        let counts={};
        try {
            if (start !== undefined && end !== undefined) {
                start = this.convertDateFormat(start);
                end = this.convertDateFormat(end);
                let db = new MySQLConnection();
                
                let sql = "SELECT status, COUNT(status) AS 'count' FROM dsiss.absns_certificate_inference WHERE vessel_id = " + vId
                    + " AND date_scheduled BETWEEN '" + start + " 00:00:00' AND '" + end + " 23:59:59'"
                    + " GROUP BY status";
                let result = await db.query(sql);
                console.log(sql);
                result.forEach(row => {
                    counts[row.status]=row.count;  
                });
                db.close();
            }
            return counts;
        } catch (error) {
            console.error('Error:', error);
            return counts;
        }
        
    }
}

module.exports = DBUtil;