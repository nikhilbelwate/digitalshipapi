var mssql = require("mssql");
//exchange:.Seaspan@dsissdatawarehouse.sql.azuresynapse.net:1433/dsissWarehouse?driver=ODBC Driver 17 for SQL Server
// config for your database
var config = {
    user: 'exchange',
    password: '.Seaspan',
    server: 'dsissdatawarehouse.sql.azuresynapse.net',
    database: 'dsissWarehouse',
    options: {
        encrypt: true, // for Azure SQL, set to true
        trustServerCertificate: false, // change to true if needed for self-signed certificates
    },
};
class DLUtil {
    static getCountOftugboat_data_raw=async ()=>{
        try{
        const pool = await new mssql.ConnectionPool(config).connect();
        const query = "select count(*) as TOTAL from bronze.tugboat_data_raw";
        const result = await pool.request().query(query);
        console.log('Connected to MS SQL Server');
        console.log(result.recordset);
        let res=Object.assign({},result.recordset);
        pool.close();
            return res;
        }catch(e){
            return e;
        }
    }
    static checkConnection = () => {
        // Create a connection pool
        try{
        const pool = new mssql.ConnectionPool(config);
        const query = "select count(*) from bronze.tugboat_data_raw";
        // Connect to the database
        pool.connect()
            .then(() => {
                console.log('Connected to SQL Server');
                pool.request()
                    .query(query)
                    .then((result) => {
                        console.log(result.recordset); // This will contain the query result

                    })
                    .catch((err) => {
                        console.error('Error executing query', err);

                    });

            })
            .catch((err) => {
                console.error('Error connecting to SQL Server', err);
            });

        // Close the connection pool when your application exits
        process.on('exit', () => {
            pool.close();
        });
    }catch(e){
        console.log(e)
    }
}
}

module.exports = DLUtil;

